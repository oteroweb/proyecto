<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'pa-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	// 'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note">Los Campos Marcado con <span class="required">*</span> Son Requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

<div class="row-fluid">
 
<div class="span4"><?php echo $form->dropDownListRow($model,'idpgar',Pa::Obtenerpgar(), array('empty' => '')); ?></div>
<div class="span4"><label>Fecha inicio*</label><?php echo $form->dropDownList($model,'anioinicio',$anioinicio ,array('options' => array(date('Y')=>array('selected'=>true)),'class'=>'span5','maxlength'=>4)); ?> </div>
<div class="span4"><?php echo $form->textFieldRow($model,'aniofinal',array('readonly'=>true, 'class'=>'span5','maxlength'=>4)); ?></div>
<div class="span4"><?php echo $form->textFieldRow($model,'planaccion',array('readonly'=>true, 'class'=>'span5','maxlength'=>250)); ?></div>




</div>


<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>








<!-- Botón para agregar filas -->








<script type="text/javascript">
	$( document ).ready(function() {
	$("#Pa_anioinicio" ).change(function() {
	    var	val= parseInt($("#Pa_anioinicio" ).val());
		var	value= parseInt($("#Pa_anioinicio" ).val())+3;
// console.log(value);
		  $("#Pa_aniofinal" ).val(value);
		  $("#Pa_planaccion" ).val(val+"-"+value);
});
});


</script>
