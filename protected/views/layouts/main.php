<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

        <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/cabecera/Recetteplus_favicon.png" type="image/x-icon" />



        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body>

        <div class="container" id="page">

            <div id="header"> 

      <!--   <div id="logo">
	  
			<!-- header -->
             <div id="header">
			   <div style="float:left;"><img src="images/logos/logo_compania.jpg" width="120px"></img></div>
               <div id="logo" ><?php echo CHtml::encode(Yii::app()->name);    ?>
			   <div class="clear"></div>
			   </div>
            <div id="mainMbMenu">



                <?php
                $this->widget('application.extensions.mbmenu.MbMenu', array(
                    'items' => array(
                        array('label' => 'Inicio', 'url' => array('/site/index')),
                        array('label' => 'Buscar', 'url' => array('/basicos/index')),
                        array('label' => 'Crear Evento', 'url' => array('/basicos/create'),
                            'items' => array(
                                array('label' => 'Listado de Eventos', 'url' => array('/subproyecto/admin')),
                            ),
                            'visible' => !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('administrador')
                        ),
                        array('label' => 'Crear Evento', 'url' => array('/subproyecto/create'),
                            'items' => array(
                                array('label' => 'Listado de Eventos', 'url' => array('/subproyecto/admin')),
                             ),
                            'visible' => !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('internos_municipios')
                        ),
                        array('label' => 'Localizacion',
                            'items' => array(
                                array('label' => 'Departamentos', 'url' => array('/departamento/admin')),
                                array('label' => 'Municipios', 'url' => array('/municipio/admin')),
                                array('label' => 'Corregimiento', 'url' => array('/corregimiento/admin')),
                            ),
                            'visible' => !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('administrador')
                        ),
                        array('label' => 'Planificacion',
                            'items' => array(
							    array('label' => 'AREA', 'url' => array('/area/admin')),
                                array('label' => 'PGAR', 'url' => array('/pgar/admin')),
                                array('label' => 'PLAN DE ACCION', 'url' => array('/pa/admin')),
								 array('label' => 'PROGRAMA CORPORACTIVO', 'url' => array('/programacorporactivo/admin')),
								  array('label' => 'PROYECTO', 'url' => array('/proyecto/admin')),
								   array('label' => 'ACTIVIDAD', 'url' => array('/actividada/admin')),
								     array('label' => 'INDICADOR', 'url' => array('/indicador/admin')),
                            ),
                            'visible' => !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('administrador')
                        ),
						 array('label' => 'Financiera',
                            'items' => array(
							    array('label' => 'Recursos', 'url' => array('/financiacion/admin')),
                               // array('label' => 'PGAR', 'url' => array('/pgar/admin')),
                               // array('label' => 'PLAN DE ACCION', 'url' => array('/pa/admin')),
                            ),
                            'visible' => !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('administrador')
                        ),
                       // array('label' => 'Cuenta', 'url' => array('/cruge/ui/editprofile'),
                           // 'visible' => ((!Yii::App()->user->IsGuest) && (Yii::App()->user->checkAccess('administrador') || Yii::App()->user->checkAccess('internos_municipios') ))),
                        
						array('label' => 'Sistema',
                            'items' => array(
							   // array('label' => 'AREA', 'url' => array('/area/admin')),
                                array('label' => 'Unidaddemedida', 'url' => array('/unidaddemedida/admin')),
                                array('label' => 'Configuraciones de sistema', 'url' => array('/cruge/ui/systemupdate')),
                            ),
                            'visible' => !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('administrador')
                        ),
						array('label' => 'Usuarios',
                            'items' => array(
                                //array('label' => 'Crear usuarios', 'url' => array('/cruge/ui/usermanagementcreate')),
                                array('label' => 'listar usuarios', 'url' => array('/cruge/ui/usermanagementadmin')),
                                array('label' => 'Asignar roles a usuarios', 'url' => array('/cruge/ui/rbacusersassignments')),
                                array('label' => 'Seciones de Usuarios', 'url' => array('/cruge/ui/sessionadmin')),
                            ),
                            'visible' => !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('administrador')
                        ),
                       // array('label' => 'Configuraciones de sistema', 'url' => array('/cruge/ui/systemupdate'),
                          //  'visible' => !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('administrador')),
                        //array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
                        //array('label'=>'Contact', 'url'=>array('/site/contact')),
                        array('label' => 'Administrar Usuarios'
                            , 'url' => Yii::app()->user->ui->userManagementAdminUrl
                            , 'visible' => !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('admin')),
                        array('label' => 'Login'
                            , 'url' => Yii::app()->user->ui->loginUrl
                            , 'visible' => Yii::app()->user->isGuest),
                        array('label' => 'Logout (' . Yii::app()->user->name . ')'
                            , 'url' => Yii::app()->user->ui->logoutUrl
                            , 'visible' => !Yii::app()->user->isGuest),
                    ),
                ));
                ?>
            </div><!-- mainmenu -->

            <?php //if (isset($this->breadcrumbs)):     ?>
            <?php
            /*    $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
              'links' => $this->breadcrumbs,
              )); */
            ?><!-- breadcrumbs -->
            <?php //endif ?>

            <?php echo $content; ?>

            <div class="clear"></div>
            <!-- webim button --><a href="http://laguajira.sisgr.com.co/chatcliente/client.php?locale=sp&amp;style=original" target="_blank" onclick="if (navigator.userAgent.toLowerCase().indexOf('opera') != - 1 & amp; & amp; window.event.preventDefault) window.event.preventDefault(); this.newWindow = window.open('http://laguajira.sisgr.com.co/chatcliente/client.php?locale=sp&amp;style=original&amp;url=' + escape(document.location.href) + '&amp;referrer=' + escape(document.referrer), 'webim', 'toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=640,height=480,resizable=1'); this.newWindow.focus(); this.newWindow.opener = window; return false;"><img src="http://laguajira.sisgr.com.co/chatcliente/b.php?i=mgreen&amp;lang=sp" border="0" width="177" height="61" alt=""/></a><!-- / webim button -->
            <div id="footer">
                Copyright &copy; <?php echo date('Y'); ?> by FUNDACION WACUA AIPA.<br/>
                Todos los derechos Reservado.<br/>
                <img src="<?php echo Yii:: app()->baseUrl . '/images/pie_pagina/Logof.png' ?>">

                    <?php // echo Yii::powered();    ?>
            </div><!-- footer -->

        </div><!-- page -->
        <?php echo Yii::app()->user->ui->displayErrorConsole(); ?>
    </body>
</html>