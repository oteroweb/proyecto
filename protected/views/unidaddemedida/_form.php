<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'unidaddemedida-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	// 'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>
<div class="row-fluid">
<div class="span8"><?php echo $form->textFieldRow($model,'unidad',array('class'=>'span5','maxlength'=>20)); ?></div>
<div class="span4"><?php echo $form->textFieldRow($model,'simbolo',array('class'=>'span5','maxlength'=>23)); ?></div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Guardar' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
