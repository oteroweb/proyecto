


<?php
$pdf = Yii::createComponent('application.extensions.MPDF57.mpdf');

$html = '
<table align="center"><tr>
<td align="center"><b>INFORMACION DEL EVENTO</b></td>
</tr>
</table>

<tr>
		<td></td> <td></td>
	</tr>




<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border-color:#aabcfe;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#669;background-color:#e8edff;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#039;background-color:#b9c9fe;}
.tg .tg-nudq{color:#000000;text-align:center}
.tg .tg-hjjq{background-color:#ffffff;color:#000000}
.tg .tg-ek65{background-color:#ffffff;color:#000000}
.tg .tg-okwu{background-color:#ffffff;color:#000000;text-align:right}
.tg .tg-qtrj{background-color:#ffffff;color:#000000;text-align:right}
</style>
<table class="tg">
  <tr>
    <th class="tg-nudq" colspan="6">Informacion Basica</th>
  </tr>
  <tr>
    <td class="tg-hjjq">Fecha</td>  <td class="tg-hjjq">' . $model->fecha . '</td>
  </tr>
  <tr>
    <td class="tg-hjjq">Codigo de Evento</td> <td class="tg-hjjq">' . $model->codevento . '</td>
  </tr>
  <tr>
    <td class="tg-hjjq">Municipio</td> <td class="tg-hjjq">' . $model->idmun0->municipio . '</td> 
  </tr>
  <tr>
    <td class="tg-hjjq">Corregimiento</td> <td class="tg-hjjq">' . $model->idcor0->correguimiento . '</td>
  </tr>
   <tr>
    <td class="tg-hjjq">Coordenada x (latitud)</td> <td class="tg-hjjq">' . $model->cordx . '</td>
  </tr>
  <tr>
    <td class="tg-hjjq">Coordenada y (longitud)</td> <td class="tg-hjjq">' . $model->cordy . '</td>
  </tr>
  <tr>
    <td class="tg-hjjq">Dias</td> <td class="tg-hjjq">' . $model->dia . '</td>
  </tr>
  <tr>
    <td class="tg-hjjq">Evento</td> <td class="tg-hjjq">' . $model->idevento0->evento . '</td>
  </tr>
  <tr>
    <td class="tg-hjjq">Calificacion del Evento</td> <td class="tg-hjjq">' . $model->cal_evento . '</td>
  </tr>
   <tr>
    <td class="tg-hjjq">Causas del Evento</td> <td class="tg-hjjq">' . $model->causa_evento . '</td>
  </tr>
  <tr>
    <td class="tg-hjjq">Vulnerabilidad</td> <td class="tg-hjjq">' . $model->vulnerabilidad->vulnerabiliad . '</td>
  </tr>
   <tr>
    <td class="tg-hjjq">Grado de Vulnerabilidad</td> <td class="tg-hjjq">' . $model->gradovul . '</td>
  </tr>
   <tr>
    <td class="tg-hjjq">Fuente de Informacion</td> <td class="tg-hjjq">' . $model->finformacion . '</td>
  </tr>
  <tr>
    <td class="tg-hjjq">Descripcion</td> <td class="tg-hjjq">' . $model->descripcion . '</td>
  </tr>
  <tr>
    <td class="tg-hjjq">Acciones para la Amenaza</td> <td class="tg-hjjq">' . $model->accionespar . '</td>
  </tr>
   <tr>
    <td class="tg-hjjq">Observaciones</td> <td class="tg-hjjq">' . $model->observaciones . '</td>
  </tr>
</table>




<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border-color:#aabcfe;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#669;background-color:#e8edff;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#039;background-color:#b9c9fe;}
.tg .tg-nudq{color:#000000;text-align:center}
.tg .tg-hjjq{background-color:#ffffff;color:#000000}
.tg .tg-ek65{background-color:#ffffff;color:#000000}
.tg .tg-okwu{background-color:#ffffff;color:#000000;text-align:right}
.tg .tg-qtrj{background-color:#ffffff;color:#000000;text-align:right}
</style>
<table class="tg">
  <tr>
    <th class="tg-nudq" colspan="6">Personas Afectadas</th>
  </tr>
  <tr>
    <td class="tg-hjjq">Muertos</td>  <td class="tg-hjjq">' . $model->Muertos . '</td>
  </tr>
  <tr>
    <td class="tg-hjjq">Heridos</td> <td class="tg-hjjq">' . $model->heridos . '</td>
  </tr>
  <tr>
    <td class="tg-hjjq">Damnificados</td> <td class="tg-hjjq">' . $model->damnificados . '</td> 
  </tr>
  <tr>
    <td class="tg-hjjq">Enfermos</td> <td class="tg-hjjq">' . $model->enfermos . '</td>
  </tr>
   <tr>
    <td class="tg-hjjq">Evacuados</td> <td class="tg-hjjq">' . $model->evacuados . '</td>
  </tr>
  <tr>
    <td class="tg-hjjq">Desaparecidos</td> <td class="tg-hjjq">' . $model->desaparecidos . '</td>
  </tr>
  <tr>
    <td class="tg-hjjq">Reubicados</td> <td class="tg-hjjq">' . $model->reubicados . '</td>
  </tr>
</table>


<tr>
		<td></td> <td></td>
	</tr>

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border-color:#aabcfe;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#669;background-color:#e8edff;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#039;background-color:#b9c9fe;}
.tg .tg-nudq{color:#000000;text-align:center}
.tg .tg-hjjq{background-color:#ffffff;color:#000000}
.tg .tg-ek65{background-color:#ffffff;color:#000000}
.tg .tg-okwu{background-color:#ffffff;color:#000000;text-align:right}
.tg .tg-qtrj{background-color:#ffffff;color:#000000;text-align:right}
</style>
<table class="tg">
  <tr>
    <th class="tg-nudq" colspan="6">Elementos Afectados</th>
  </tr>
  <tr>
    <td class="tg-hjjq">Viviendas destruidas (Cantidad)</td>  <td class="tg-hjjq">' . $model->vdestruidas . '</td>
  </tr>
  <tr>
    <td class="tg-hjjq">Viviendas afectadas (Cantidad)</td> <td class="tg-hjjq">' . $model->vafectadas . '</td>
  </tr>
  <tr>
    <td class="tg-hjjq">Semovientes (Cantidad)</td> <td class="tg-hjjq">' . $model->semovientes . '</td> 
  </tr>
  <tr>
    <td class="tg-hjjq">Cultivos (Hectareas)</td> <td class="tg-hjjq">' . $model->cultivos . '</td>
  </tr>
   <tr>
    <td class="tg-hjjq">Vias afectadas (Kilometros)</td> <td class="tg-hjjq">' . $model->viasafectadas . '</td>
  </tr>
  <tr>
    <td class="tg-hjjq">Cobertura Vegetal (Hectareas)</td> <td class="tg-hjjq">' . $model->bosque . '</td>
  </tr>
  <tr>
    <td class="tg-hjjq">Centros Educativos (Cantidad)</td> <td class="tg-hjjq">' . $model->ceducativo . '</td>
  </tr>
  <tr>
    <td class="tg-hjjq">Centros hospitalarios (Cantidad)</td> <td class="tg-hjjq">' . $model->chospital . '</td>
  </tr>
  <tr>
    <td class="tg-hjjq">Valor perdida (Aproximada)</td> <td class="tg-hjjq">' . $model->valorperdida . '</td>
  </tr>
</table>






<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border-color:#aabcfe;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#669;background-color:#e8edff;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#039;background-color:#b9c9fe;}
.tg .tg-nudq{color:#000000;text-align:center}
.tg .tg-hjjq{background-color:#ffffff;color:#000000}
.tg .tg-ek65{background-color:#ffffff;color:#000000}
.tg .tg-okwu{background-color:#ffffff;color:#000000;text-align:right}
.tg .tg-qtrj{background-color:#ffffff;color:#000000;text-align:right}
</style>
<table class="tg">
<tr>
    <th class="tg-nudq" colspan="6">ANEXOS</th>
  </tr>
  <tr>
    <td class="tg-hjjq">Imagen 1</td> <td class="tg-hjjq"> <img src="' . Yii::app()->request->baseUrl . '/images/basicos/' . $model->imagen1 . '"/height="50" width="50%"></td>
  </tr>
  <tr>
    <td class="tg-hjjq">Imagen 2</td> <td class="tg-hjjq"> <img src="' . Yii::app()->request->baseUrl . '/images/basicos/' . $model->imagen2 . '"/height="50" width="50%"></td>
  </tr>
  <tr>
    <td class="tg-hjjq">Imagen 3</td> <td class="tg-hjjq"> <img src="' . Yii::app()->request->baseUrl . '/images/basicos/' . $model->imagen3 . '"/height="50" width="50%"></td>
  </tr>
</table>





';
$header = '<div><center><img src="' . Yii::app()->request->baseUrl . '/images/cabecera/colorcorpo2.jpg"/ height="65" width="100%"></center></div>';
$mpdf = new mPDF('win-1252', 'LETTER', '', '', 15, 15, 25, 12, 5, 7);
$mpdf->SetHTMLHeader($header);
$mpdf->SetFooter(' {DATE j/m/Y}|Página {PAGENO}/{nbpg}|Eventos de Riesgo');
$mpdf->WriteHTML($html);
$mpdf->Output('Eventos de Riesgos' . '.pdf', 'D');

exit;
?>







?>
