<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<legend><h5>Informacion Requerida</h5></legend>
			<?php echo $form->textFieldRow($model,'fecha',array('class'=>'span5','maxlength'=>4)); ?>

		<?php echo $form->textFieldRow($model,'fechafinal',array('class'=>'span5','maxlength'=>4)); ?>

		<?php echo $form->textFieldRow($model,'detalle',array('class'=>'span5','maxlength'=>250)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>




