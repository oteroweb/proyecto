<?php

/**
 * This is the model class for table "subproyecto".
 *
 * The followings are the available columns in table 'subproyecto':
 * @property integer $id
 * @property integer $codigo
 * @property string $fechaderegistro
 * @property integer $radicado
 * @property string $fecharadicado
 * @property string $nombre
 * @property string $detalle
 * @property string $objetivogeneral
 * @property integer $idlocalizacion
 * @property integer $poblacionben
 * @property integer $idunidad
 * @property integer $meta
 * @property integer $idrecursos
 * @property integer $idpa
 * @property integer $idpc
 * @property integer $idproy
 * @property integer $idact
 * @property integer $idformulador
 * @property double $valortotal
 * @property double $valorsolicitado
 *
 * The followings are the available model relations:
 * @property Objespecificos[] $objespecificoses
 */
class Subproyecto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'subproyecto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('codigo, fechaderegistro, radicado, fecharadicado, nombre, detalle, objetivogeneral, idlocalizacion, poblacionben, idunidad, meta, idrecursos, idpa, idpc, idproy, idact, idformulador, valortotal, valorsolicitado', 'required'),
			array('codigo, radicado, idlocalizacion, poblacionben, idunidad, meta, idrecursos, idpa, idpc, idproy, idact, idformulador', 'numerical', 'integerOnly'=>true),
			array('valortotal, valorsolicitado', 'numerical'),
			array('nombre', 'length', 'max'=>250),
			array('detalle', 'length', 'max'=>500),
			array('objetivogeneral', 'length', 'max'=>200),
			/*
			//Example username
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u',
                 'message'=>'Username can contain only alphanumeric 
                             characters and hyphens(-).'),
          	array('username','unique'),
          	*/
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, codigo, fechaderegistro, radicado, fecharadicado, nombre, detalle, objetivogeneral, idlocalizacion, poblacionben, idunidad, meta, idrecursos, idpa, idpc, idproy, idact, idformulador, valortotal, valorsolicitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'objespecificoses' => array(self::HAS_MANY, 'Objespecificos', 'idproyecto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'codigo' => 'Codigo',
			'fechaderegistro' => 'Fechaderegistro',
			'radicado' => 'Radicado',
			'fecharadicado' => 'Fecharadicado',
			'nombre' => 'Nombre',
			'detalle' => 'Detalle',
			'objetivogeneral' => 'Objetivogeneral',
			'idlocalizacion' => 'Idlocalizacion',
			'poblacionben' => 'Poblacionben',
			'idunidad' => 'Idunidad',
			'meta' => 'Meta',
			'idrecursos' => 'Idrecursos',
			'idpa' => 'Idpa',
			'idpc' => 'Idpc',
			'idproy' => 'Idproy',
			'idact' => 'Idact',
			'idformulador' => 'Idformulador',
			'valortotal' => 'Valortotal',
			'valorsolicitado' => 'Valorsolicitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('codigo',$this->codigo);
		$criteria->compare('fechaderegistro',$this->fechaderegistro,true);
		$criteria->compare('radicado',$this->radicado);
		$criteria->compare('fecharadicado',$this->fecharadicado,true);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('detalle',$this->detalle,true);
		$criteria->compare('objetivogeneral',$this->objetivogeneral,true);
		$criteria->compare('idlocalizacion',$this->idlocalizacion);
		$criteria->compare('poblacionben',$this->poblacionben);
		$criteria->compare('idunidad',$this->idunidad);
		$criteria->compare('meta',$this->meta);
		$criteria->compare('idrecursos',$this->idrecursos);
		$criteria->compare('idpa',$this->idpa);
		$criteria->compare('idpc',$this->idpc);
		$criteria->compare('idproy',$this->idproy);
		$criteria->compare('idact',$this->idact);
		$criteria->compare('idformulador',$this->idformulador);
		$criteria->compare('valortotal',$this->valortotal);
		$criteria->compare('valorsolicitado',$this->valorsolicitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Subproyecto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function beforeSave() 
    {
        $userId=0;
		if(null!=Yii::app()->user->id) $userId=(int)Yii::app()->user->id;
		
		if($this->isNewRecord)
        {           
                        						
        }else{
                        						
        }

        
        	// NOT SURE RUN PLEASE HELP ME -> 
        	//$from=DateTime::createFromFormat('d/m/Y',$this->fechaderegistro);
        	//$this->fechaderegistro=$from->format('Y-m-d');
        	
        	// NOT SURE RUN PLEASE HELP ME -> 
        	//$from=DateTime::createFromFormat('d/m/Y',$this->fecharadicado);
        	//$this->fecharadicado=$from->format('Y-m-d');
        	
        return parent::beforeSave();
    }

    public function beforeDelete () {
		$userId=0;
		if(null!=Yii::app()->user->id) $userId=(int)Yii::app()->user->id;
                                
        return false;
    }

    public function afterFind()    {
         
        	// NOT SURE RUN PLEASE HELP ME -> 
        	//$from=DateTime::createFromFormat('Y-m-d',$this->fechaderegistro);
        	//$this->fechaderegistro=$from->format('d/m/Y');
        	
        	// NOT SURE RUN PLEASE HELP ME -> 
        	//$from=DateTime::createFromFormat('Y-m-d',$this->fecharadicado);
        	//$this->fecharadicado=$from->format('d/m/Y');
        	
        parent::afterFind();
    }
	
		
	public function defaultScope()
    {
    	/*
    	//Example Scope
    	return array(
	        'condition'=>"deleted IS NULL ",
            'order'=>'create_time DESC',
            'limit'=>5,
        );
        */
        $scope=array();

        
        return $scope;
    }
}
