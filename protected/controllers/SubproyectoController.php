<?php

class SubproyectoController extends Controller
{
	
	
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
		
	public $layout='//layouts/column1';		
		/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
						
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
						
		);
	}
	
		/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','export','import','editable','toggle','calendar', 'calendarEvents', ),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
		
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		
		if(isset($_GET['asModal'])){
			$this->renderPartial('view',array(
				'model'=>$this->loadModel($id),
			));
		}
		else{
						
			$this->render('view',array(
				'model'=>$this->loadModel($id),
			));
			
		}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
				
		$model=new Subproyecto;
		$model2= new Objespecificos;

		$modelx= new Objespecificos;
		$model3=new Actividad;
		$formulador= new Formulador;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Subproyecto']))
		{
			$transaction = Yii::app()->db->beginTransaction();
			try{
				$messageType='warning';
				$message = "There are some errors ";
				$model->attributes=$_POST['Subproyecto'];
				//$uploadFile=CUploadedFile::getInstance($model,'filename');
				if($model->save()){
					if(isset($_POST['Objespecificos']))
						{
				$objes=$_POST['Objespecificos'];
				$valobjespe=count($_POST['Objespecificos']);
					for ($i=0; $i < $valobjespe; $i++) { 
	  					$model2= new Objespecificos;
	                   	$model2->idproyecto = $model->idproyecto;
	                  	$model2->nomobjespecifico = $_POST['Objespecificos'][$i]["nomobjespecifico"];
	                  	$model2->producto = $_POST['Objespecificos'][$i]["producto"];;
	                   	$model2->save();
							if(isset($_POST['Objespecificos'][$i]['actividad']))
								{ 
									$acts=count($_POST['Objespecificos'][0]['actividad']);
									for ($j=0; $j < $acts; $j++) { 
									$model3=new Actividad;
									$model3->idobjespecifico=$model2->idobjespecifico;
									$model3->actividad = $_POST['Objespecificos'][$i]["actividad"][$j];
									$model3->save();
									}

								}
					}
                 


									}
					$messageType = 'success';
					$message = "<strong>Well done!</strong> You successfully create data ";


			
					/*
					$model2 = Subproyecto::model()->findByPk($model->id);						
					if(!empty($uploadFile)) {
						$extUploadFile = substr($uploadFile, strrpos($uploadFile, '.')+1);
						if(!empty($uploadFile)) {
							if($uploadFile->saveAs(Yii::app()->basePath.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'subproyecto'.DIRECTORY_SEPARATOR.$model2->id.DIRECTORY_SEPARATOR.$model2->id.'.'.$extUploadFile)){
								$model2->filename=$model2->id.'.'.$extUploadFile;
								$model2->save();
								$message .= 'and file uploded';
							}
							else{
								$messageType = 'warning';
								$message .= 'but file not uploded';
							}
						}						
					}
					*/
					$transaction->commit();
					Yii::app()->user->setFlash($messageType, $message);
					$this->redirect(array('view','id'=>$model->id));
				}				
			}
			catch (Exception $e){
				$transaction->rollBack();
				Yii::app()->user->setFlash('error', "{$e->getMessage()}");
				//$this->refresh();
			}
			
		}

		$this->render('create',array(
			'model'=>$model,
			'model2'=>$model2,
			'formulador'=>$formulador,
					));
		
				
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Subproyecto']))
		{
			$messageType='warning';
			$message = "There are some errors ";
			$transaction = Yii::app()->db->beginTransaction();
			try{
				$model->attributes=$_POST['Subproyecto'];
				$messageType = 'success';
				$message = "<strong>Well done!</strong> You successfully update data ";

				/*
				$uploadFile=CUploadedFile::getInstance($model,'filename');
				if(!empty($uploadFile)) {
					$extUploadFile = substr($uploadFile, strrpos($uploadFile, '.')+1);
					if(!empty($uploadFile)) {
						if($uploadFile->saveAs(Yii::app()->basePath.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'subproyecto'.DIRECTORY_SEPARATOR.$model->id.DIRECTORY_SEPARATOR.$model->id.'.'.$extUploadFile)){
							$model->filename=$model->id.'.'.$extUploadFile;
							$message .= 'and file uploded';
						}
						else{
							$messageType = 'warning';
							$message .= 'but file not uploded';
						}
					}						
				}
				*/

				if($model->save()){
					$transaction->commit();
					Yii::app()->user->setFlash($messageType, $message);
					$this->redirect(array('view','id'=>$model->id));
				}
			}
			catch (Exception $e){
				$transaction->rollBack();
				Yii::app()->user->setFlash('error', "{$e->getMessage()}");
				// $this->refresh(); 
			}

			$model->attributes=$_POST['Subproyecto'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
					));
		
			}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		/*
		$dataProvider=new CActiveDataProvider('Subproyecto');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
		*/
		
		$model=new Subproyecto('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Subproyecto']))
			$model->attributes=$_GET['Subproyecto'];

		$this->render('index',array(
			'model'=>$model,
					));
		
			}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		
		$model=new Subproyecto('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Subproyecto']))
			$model->attributes=$_GET['Subproyecto'];

		$this->render('admin',array(
			'model'=>$model,
					));
		
			}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Subproyecto the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Subproyecto::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Subproyecto $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='subproyecto-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionExport()
    {
        $model=new Subproyecto;
		$model->unsetAttributes();  // clear any default values
		if(isset($_POST['Subproyecto']))
			$model->attributes=$_POST['Subproyecto'];

		$exportType = $_POST['fileType'];
        $this->widget('ext.heart.export.EHeartExport', array(
            'title'=>'List of Subproyecto',
            'dataProvider' => $model->search(),
            'filter'=>$model,
            'grid_mode'=>'export',
            'exportType'=>$exportType,
            'columns' => array(
	                
					'id',
					'codigo',
					'fechaderegistro',
					'radicado',
					'fecharadicado',
					'nombre',
					'detalle',
					'objetivogeneral',
					'idlocalizacion',
					'poblacionben',
					'idunidad',
					'meta',
					'idrecursos',
					'idpa',
					'idpc',
					'idproy',
					'idact',
					'idformulador',
					'valortotal',
					'valorsolicitado',
	            ),
        ));
    }

    /**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionImport()
	{
		
		$model=new Subproyecto;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Subproyecto']))
		{
			if (!empty($_FILES)) {
				$tempFile = $_FILES['Subproyecto']['tmp_name']['fileImport'];
				$fileTypes = array('xls','xlsx'); // File extensions
				$fileParts = pathinfo($_FILES['Subproyecto']['name']['fileImport']);
				if (in_array(@$fileParts['extension'],$fileTypes)) {

					Yii::import('ext.heart.excel.EHeartExcel',true);
	        		EHeartExcel::init();
	        		$inputFileType = PHPExcel_IOFactory::identify($tempFile);
					$objReader = PHPExcel_IOFactory::createReader($inputFileType);
					$objPHPExcel = $objReader->load($tempFile);
					$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
					$baseRow = 2;
					$inserted=0;
					$read_status = false;
					while(!empty($sheetData[$baseRow]['A'])){
						$read_status = true;						
						$id=  $sheetData[$baseRow]['A'];
						$codigo=  $sheetData[$baseRow]['B'];
						$fechaderegistro=  $sheetData[$baseRow]['C'];
						$radicado=  $sheetData[$baseRow]['D'];
						$fecharadicado=  $sheetData[$baseRow]['E'];
						$nombre=  $sheetData[$baseRow]['F'];
						$detalle=  $sheetData[$baseRow]['G'];
						$objetivogeneral=  $sheetData[$baseRow]['H'];
						$idlocalizacion=  $sheetData[$baseRow]['I'];
						$poblacionben=  $sheetData[$baseRow]['J'];
						$idunidad=  $sheetData[$baseRow]['K'];
						$meta=  $sheetData[$baseRow]['L'];
						$idrecursos=  $sheetData[$baseRow]['M'];
						$idpa=  $sheetData[$baseRow]['N'];
						$idpc=  $sheetData[$baseRow]['O'];
						$idproy=  $sheetData[$baseRow]['P'];
						$idact=  $sheetData[$baseRow]['Q'];
						$idformulador=  $sheetData[$baseRow]['R'];
						$valortotal=  $sheetData[$baseRow]['S'];
						$valorsolicitado=  $sheetData[$baseRow]['T'];

						$model2=new Subproyecto;
						$model2->id=  $id;
						$model2->codigo=  $codigo;
						$model2->fechaderegistro=  $fechaderegistro;
						$model2->radicado=  $radicado;
						$model2->fecharadicado=  $fecharadicado;
						$model2->nombre=  $nombre;
						$model2->detalle=  $detalle;
						$model2->objetivogeneral=  $objetivogeneral;
						$model2->idlocalizacion=  $idlocalizacion;
						$model2->poblacionben=  $poblacionben;
						$model2->idunidad=  $idunidad;
						$model2->meta=  $meta;
						$model2->idrecursos=  $idrecursos;
						$model2->idpa=  $idpa;
						$model2->idpc=  $idpc;
						$model2->idproy=  $idproy;
						$model2->idact=  $idact;
						$model2->idformulador=  $idformulador;
						$model2->valortotal=  $valortotal;
						$model2->valorsolicitado=  $valorsolicitado;

						try{
							if($model2->save()){
								$inserted++;
							}
						}
						catch (Exception $e){
							Yii::app()->user->setFlash('error', "{$e->getMessage()}");
							//$this->refresh();
						} 
						$baseRow++;
					}	
					Yii::app()->user->setFlash('success', ($inserted).' row inserted');	
				}	
				else
				{
					Yii::app()->user->setFlash('warning', 'Wrong file type (xlsx, xls, and ods only)');
				}
			}


			$this->render('admin',array(
				'model'=>$model,
			));
		}
		else{
			$this->render('admin',array(
				'model'=>$model,
			));
		}
	}

	public function actionEditable(){
		Yii::import('bootstrap.widgets.TbEditableSaver'); 
	    $es = new TbEditableSaver('Subproyecto'); 
			    $es->update();
	}

	public function actions()
	{
    	return array(
        		'toggle' => array(
                	'class'=>'bootstrap.actions.TbToggleAction',
                	'modelName' => 'Subproyecto',
        		)
    	);
	}

	
	public function actionCalendar()
	{
		$model=new Subproyecto('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Subproyecto']))
			$model->attributes=$_GET['Subproyecto'];
		$this->render('calendar',array(
			'model'=>$model,
		));	
	}

	public function actionCalendarEvents()
	{	 	
	 	$items = array();
	 	$model=Subproyecto::model()->findAll();	
		foreach ($model as $value) {
			$items[]=array(
				'id'=>$value->id,
								
				//'color'=>'#CC0000',
	        	//'allDay'=>true,
	        	'url'=>'#',
			);
		}
	    echo CJSON::encode($items);
	    Yii::app()->end();
	}

	
}
