-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-08-2015 a las 05:19:50
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `proyecto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividada`
--

CREATE TABLE IF NOT EXISTS `actividada` (
`id` int(11) NOT NULL,
  `idpro` int(11) NOT NULL,
  `codact` varchar(20) NOT NULL,
  `actividad` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `actividada`
--

INSERT INTO `actividada` (`id`, `idpro`, `codact`, `actividad`) VALUES
(1, 3, '111', 'Ajustes de los POMCAS formulados de las cuencas priorizadas en la jurisdicción de la Corporación de conformidad con el Decreto 1640 de 2012. '),
(2, 3, '112', 'Asesorar a los municipios en la revisión y ajustes de los POT.'),
(3, 3, '113', 'Asesorar a los municipios en la formulación de los SIGAM y SISBIM.'),
(4, 3, '114', 'Seguimiento y evaluación a la ejecución de los POT de los municipios.'),
(5, 3, '115', 'Cartografía temática (1:25000) recursos naturales, ecosistemas continentales, costeros y marinos de los municipios de La Guajira.'),
(6, 3, '116', 'Georeferenciación de proyectos y actividades ejecutas por la Corporación.'),
(7, 3, '117', 'Asesoría técnica a los municipios para incorporar dentro del componente ambiental de los POT los ecosistemas marinos y costeros.'),
(8, 3, '118', 'Observatorio Ambiental (diseño e implementación de un sistema de seguimiento y monitoreo de la calidad de los recursos naturales y el ambiente).'),
(9, 3, '119', 'Delimitación y zonificación del páramo seco Cerro Pintao y humedales a escala 1:25000.'),
(10, 3, '1110', 'Implantación del Sistema de Información Ambiental (SIA).'),
(11, 3, '121', 'Acompañamiento al Consejo Departamental y Municipales de Gestión del Riesgo y Desastres.'),
(12, 7, '122', 'Desarrollar acciones de mitigación al cambio climático y protección de coberturas boscosas y/o de suelos degradados a través de la gestión y ejecución de acciones piloto en proyectos REDD.'),
(13, 7, '123', 'Desarrollar proyectos de captura de carbono en biomasa aerea en diferente cobertura vegetal.'),
(14, 7, '124', 'Fortalecimiento del sistema de alertas tempranas - SAT'),
(15, 7, '125', 'Monitoreo, obtención de datos y procesamiento al sistema de alerta temprana (análoga) en deslizamientos e inundaciones.'),
(16, 7, '126', 'Zonificación de riesgos de zonas costeras a escala 1:25000'),
(17, 7, '127', 'Estudio de amenaza y vulnerabilidad ambiental ante fenómenos naturales.'),
(18, 7, '128', 'Asesoría asistencia técnica a los municipios en el proceso de revisión, ajuste o reformulación de los POT con énfasis en la inclusión de las determinantes ambientales y la gestión del riesgo. '),
(19, 7, '129', 'Asesoría y asistencia técnica para la formulación de planes sectoriales con incorporación de política de adaptación al cambio climático.'),
(20, 7, '1210', 'Construcción de obras de control de inundaciones, de erosión, de caudales, de escorrentía, rectificación y manejo de cauces, obras de geotecnia, regulación de cauces y corrientes de agua y demás obras para el manejo de aguas.    '),
(21, 7, '131', 'Fortalecimiento del banco de proyectos.'),
(22, 7, '132', 'Fortalecer los Programas y Proyectos de Ciencia, innovación y tecnología para las investigaciones y estudios que sustenten científicamente la dimensión ambiental incorporada a los programas de desarrollo económico y social.'),
(23, 8, '133', 'Seguimiento a la sostenibilidad de los proyectos implementados en el marco de las convocatorias de COLCIENCIAS - IDEAS PARA EL CAMBIO.'),
(24, 8, '134', 'Formulación de proyectos ambientales para acceder a recursos nacionales e internacionales.'),
(25, 8, '241', 'Obras de infraestructura para captación y/o almacenamiento de agua a las comunidades indígenas y negras.'),
(26, 8, '242', 'Formular planes de ordenamiento del recurso hídrico y reglamentar corrientes y vertimientos.'),
(27, 8, '243', 'Actualizar el sistema de información del recurso hÍdrico (SIRH).'),
(28, 8, '244', 'Evaluación y seguimiento a los programas de ahorro y uso eficiente de agua (PAUEA).'),
(29, 8, '245', 'Identificar zonas estratégicas para el aprovechamiento de aguas subterráneas. '),
(30, 9, '246', 'Definir la oferta y demanda del recurso hídrico subterráneo.'),
(31, 9, '247', 'Construir una guía metodológica de buenas prácticas de administración, exploración y perforación de agua subterránea.  '),
(32, 9, '248', 'Formular e implementar planes de manejo de aguas subterráneas (Acuíferos).'),
(33, 9, '249', 'Continuar la implementación del proyecto de protección integrada de agua subterránea (PPIAS) en el municipio de Maicao. '),
(34, 9, '251', 'Realizar monitoreo del recurso hídrico relacionado en los trámites y proyectos a los que se otorguen licencias, permisos y/o concesiones ambientales.'),
(35, 9, '252', 'Determinar carga contaminante para cobro de tasas retributivas'),
(36, 9, '253', 'Determinar la calidad del agua y el estado de contaminación de cuerpos de agua de interés ecológico y ambiental.'),
(37, 9, '254', 'Realizar monitoreo a corrientes hídricas abastecedoras de acueductos.'),
(38, 9, '255', 'Sostenibilidad y ampliación de la acreditación del laboratorio.'),
(39, 9, '256', 'Determinar linea base de carbón, hidrocarburos y plaguicidas en las corrientes principales de las cuencas hidrográficas.'),
(40, 10, '361', 'Formular e implementar planes de manejo de áreas protegidas regionales.'),
(41, 10, '362', 'Administración y manejo de áreas protegidas y de interés estratégico para la conservación.'),
(42, 10, '363', 'Desarrollar acciones para la protección y conservación de áreas protegidas locales y de interés estratégico (convenios con municipios, ONG, propietarios e instituciones). '),
(43, 10, '364', 'Coordinar con la Unidad de Parques Nacionales para la definición y acciones de conservación de zonas de amortiguamiento.'),
(44, 10, '365', 'Acciones para la sostenibilidad de la serranía de La Macuira, S.N.S.M. y la serranía de Perijá en jurisdicción de Corpoguajira.'),
(45, 10, '366', 'Definir y establecer corredor biológico SNSM y Perijá. '),
(46, 11, '367', 'Formulación e implementación de planes de manejo de páramos y humedales.'),
(47, 11, '368', 'Formulación de planes de protección de costa frente a problemas de erosión.'),
(48, 11, '369', 'Implementación del plan de acción regional de lucha contra la desertificación y la sequía.'),
(49, 11, '3610', 'Adopción e implementación del plan de manejo de manglares.'),
(50, 11, '3611', 'Adopción e implementación de los planes de manejo de las Unidades Ambientales Costeras.'),
(51, 11, '3612', 'Promover la sensibilización y socialización de estudios o investigaciones sobre la degradación y el manejo de los suelos.'),
(52, 11, '3613', 'Contribuir a la preservación e incremento de los servicios ecosistémicos de los suelos mediante la investigación y la transferencia de buenas prácticas de manejo. '),
(53, 11, '3614', 'Ejercicios de valoración de bienes y servicios ambientales.'),
(54, 11, '3615', 'Saneamiento de zonas estratégicas para la conservación (adquisición de predios).'),
(55, 11, '371', 'Ajuste y adopción al plan de ordenación forestal del departamento de La Guajira.'),
(56, 11, '372', 'Promover la conservación y el uso sostenible del bosque, la reforestación, la restauración ecológica y el establecimiento de plantaciones productoras.'),
(57, 11, '373', 'Asesoría y asistencia técnica al establecimiento de bosque forestal productivo.'),
(58, 11, '374', 'Estudio para la delimitación de la tasa de deforestación.'),
(59, 11, '375', 'Fortalecer la articulación interinstitucional para el seguimiento, monitoreo y control eficiente del tráfico ilegal de especies.'),
(60, 11, '376', 'Implementación de la base cientifica y capacidad técnica para la valoración, manejo y disposición de especimenes decomisados.'),
(61, 11, '377', 'Formulación e implementación de programas de conservación de fauna y flora endémica, amenazada y con alta presión en el departamento.'),
(62, 11, '378', 'Gestión para el ajuste del diseño e implementación del jardín botánico.'),
(63, 12, '379', 'Formulación y/o implementación de programas para la conservación de especies migratorias marino costeras.'),
(64, 12, '3710', 'Promoción a la conservación y uso sostenible de los recursos naturales mediante el conocimiento del uso potencial de la biodiversidad nativa y la identificación de especies promisorias.'),
(65, 13, '381', 'Impulsar emprendimiento verde en las actividades económicas de La Guajira fortaleciendo la producción y el consumo sostenible.'),
(66, 13, '382', 'Promocionar los productos y servicios amigables con el ambiente.'),
(67, 13, '383', 'Participar en ferias y eventos comerciales que permitan promocionar los productos y servicios amigables con el medio ambiente.'),
(68, 13, '384', 'Gestionar y promocionar estrategias de instrumentos económicos, tributarios y financieros con el fin de promover el uso sostenible de la biodiversidad.'),
(69, 13, '385', 'Apoyo y fortalecimiento a la medicina, usos y costumbres tradicionales de las Minorías étnicas y comunidades negras.'),
(70, 14, '491', 'Apoyar jornadas de arborización urbana.'),
(71, 14, '492', 'Determinar la tasa de arborización en la zona urbana de las ciudades de Riohacha y Maicao.'),
(72, 14, '493', 'Asesoría y asistencia técnica a los municipios para la formulación de los planes locales de arborización.'),
(73, 14, '494', 'Proyectos de recuperación participativa de zonas verdes.'),
(74, 14, '495', 'Proyectos de recuperación participativa para la gestión ambiental urbana.'),
(75, 14, '496', 'Promover la recuperación de humedales urbanos y/o espacios verdes de la ciudad (Laguna Salá, humedal 15 de mayo y La Esperanza en Riohacha; Laguna Washington en Maicao; Sequia de Penso en Fonseca; Parque Ecológico en San Juan; Laguna de El Molino).'),
(76, 14, '497', 'Asesoría y asistencia técnica para el funcionamiento adecuado de los rellenos sanitarios del norte, sur del departamento y Riohacha. '),
(77, 14, '498', 'Asistencia y asesoría técnica para el manejo integral de los residuos sólidos y líquidos del sur de La Guajira. '),
(78, 14, '499', 'Promover alternativas tecnológicas para la disposición final de los residuos sólidos.'),
(79, 14, '4910', 'Implementación de convenios donde se promueva el reciclaje de residuos orgánicos e inorgánicos.'),
(80, 14, '4911', 'Asesoría y asistencia técnica en la construcción de escombreras.'),
(81, 15, '4101', 'Elaboración de estudios de Evaluaciones Estratégicas en sectores locomotoras de viviendas, infraestructura, agropecuarios, minería y gestión de los residuos sólidos.'),
(82, 15, '4102', 'Implementación de estrategias para el control de explotación ilícita de minerales y recursos naturales. '),
(83, 15, '4103', 'Promover la formalización y legalización de la pequeña minería.'),
(84, 15, '4104', 'Seguimiento a la implementación del sistema Medidas Sanitarias y Fitosanitarias "MSF".'),
(85, 15, '4105', 'Convenio de producción mas limpia con los sectores productivos.'),
(86, 15, '4106', 'Identificar y valorar los impactos ambientales y su contribución al cambio climático por las actividades productivas.'),
(87, 16, '4111', 'Fortalecer el Sistema de Vigilancia de la Calidad del Aire mediante el control y monitoreo de emisiones de fuentes móviles en la jurisdicción de la Corporación.'),
(88, 16, '4112', 'Control de emisiones atmosféricas de fuentes móviles.'),
(89, 16, '4113', 'Carga de contaminación atmosférica reducida por proyectos relacionados con control de contaminación atmosférica implementados (ug/m3).'),
(90, 16, '4114', 'Seguimiento a la implementación de planes de descontaminación auditiva.'),
(91, 16, '4115', 'Monitoreo de la calidad del aire en el corredor minero.'),
(92, 16, '4116', 'Monitoreo y seguimiento al transporte de carbón.'),
(93, 16, '4117', 'Elaboración de un estudio de contaminación visual.'),
(94, 16, '4118', 'Realizar estudio de contaminación por los olores ofensivos en el departamento.'),
(95, 16, '4119', 'Realizar un estudio técnico que permita promover una norma regional para establecer los limites permisibles en las emisiones atmosféricas.'),
(96, 16, '41110', 'Red de monitoreo de la calidad del aire.'),
(97, 17, '5121', 'Impulsar la implementación de la Educación Ambiental en la básica primaria, secundaria y universitaria (cátedra ambiental)'),
(98, 17, '5122', 'Asesorar y consolidar PRAES asociados a temas de cambio climático, gestión del riesgo, gestión ambiental urbana, biodiversidad y recurso hídrico.'),
(99, 17, '5123', 'Promover la conformación y el fortalecimiento de los comités técnicos municipales y departamentales interinstitucionales de educación ambiental (CIDEA)'),
(100, 17, '5124', 'Asistencia técnica a la formulación de Proyectos de educación ambiental universitarios (PRAUS).'),
(101, 17, '5125', 'Promover acciones de gestión de educación ambiental participativa con comunidades y minorías étnicas.'),
(102, 17, '5126', 'Formular e implementar una estrategia pedagógica para la protección y conservación del ambiente desde la cosmovisión de las comunidades indígenas y negras.'),
(103, 17, '5127', 'Promover acciones para la divulgación de estrategias pedagógicas de la Política de Gestión Ambiental Urbana (PGAU) y de la Política Nacional para la Gobernanza y la Cultura del Agua (PNGCA) en los sectores productivos del departamento.'),
(104, 17, '5128', 'Fortalecimiento del centro de documentación ambiental.'),
(105, 18, '5131', 'Impulsar la formulación de Proyectos Ciudadanos en Educación Ambiental (PROCEDAS).'),
(106, 18, '5132', 'Capacitación sobre mecanismos de participación ambiental ciudadana, veedurias ambientales.'),
(107, 18, '5133', 'Promover la realización de eventos de ciencia, educación y participación ambiental.'),
(108, 18, '5134', 'Fomentar el desarrollo de promotores ambientales desde el servicio social ambiental obligatorio.'),
(109, 18, '5135', 'Talleres de capacitación para la prevención, control y manejo de incendios forestales.'),
(110, 18, '5136', 'Establecer la gestión de la Corporación en la formación de capital social.'),
(111, 19, '6141', 'Seguimiento, monitoreo y control a las licencias, permisos ambientales, salvoconductos, concesiones y demás autorizaciones otorgadas, así como las medidas de compensación impuestas.'),
(112, 19, '6142', 'Tramites de Licencias Ambientales.'),
(113, 19, '6143', 'Trámites de Concesión de Aguas.'),
(114, 19, '6144', 'Trámites de Permisos de Vertimientos.'),
(115, 19, '6145', 'Trámites de Aprovechamiento Forestal Persistente.'),
(116, 19, '6146', 'Evaluación de Estudios de Impacto y planes de manejo Ambiental y otros documentos de control y seguimiento.'),
(117, 19, '6147', 'Seguimiento a la gestión e implementación de los PGIRS, RESPEL y PSMV.'),
(118, 19, '6148', 'Control de la disposición de los residuos sólidos en los municipios.'),
(119, 19, '6149', 'Control a botaderos cielo abierto.'),
(120, 19, '61410', 'Generación base de datos de infractores ambientales en el departamento de La Guajira.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anexo`
--

CREATE TABLE IF NOT EXISTS `anexo` (
`id` int(11) NOT NULL,
  `idsuproyecto` int(11) NOT NULL,
  `tipodeanexo` varchar(230) NOT NULL,
  `anexo` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area`
--

CREATE TABLE IF NOT EXISTS `area` (
  `id` int(11) NOT NULL,
  `Area` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `area`
--

INSERT INTO `area` (`id`, `Area`) VALUES
(100, 'Asamblea Corporativa'),
(200, 'Consejo Directivo'),
(300, 'Dirección General'),
(310, 'Oficina Asesora de Planeacion '),
(320, 'Oficina Asesora Jurídica '),
(330, 'Secretaria General'),
(331, 'Grupo Gestión Financiera'),
(332, 'Gestión Administrativa'),
(333, 'Gestión Documental'),
(340, 'Subdireccion de Calidad Ambiental'),
(341, 'Licencias y Tramites Ambientales'),
(342, 'Laboratorio Ambiental'),
(343, 'Grupo de Control y Monitoreo'),
(344, 'Dirección Territorial del Sur'),
(350, 'Subdireccion de Gestion Ambiental'),
(351, 'Grupo de Ecosistemas y Biodiversidad'),
(352, 'Grupo Administración y Aprovechamiento de Aguas'),
(353, 'Grupo de Educación Ambiental'),
(400, 'Organismos de Asesoría y Coordinación ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `corregimiento`
--

CREATE TABLE IF NOT EXISTS `corregimiento` (
  `id` int(11) NOT NULL,
  `correguimiento` varchar(30) NOT NULL,
  `idmun` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `corregimiento`
--

INSERT INTO `corregimiento` (`id`, `correguimiento`, `idmun`) VALUES
(44001000, 'RIOHACHA', 44001),
(44001001, 'ARROYO ARENA', 44001),
(44001002, 'BARBACOA', 44001),
(44001003, 'CAMARONES', 44001),
(44001004, 'CASCAJALITO', 44001),
(44001005, 'COTOPRIX', 44001),
(44001008, 'GALÁN', 44001),
(44001011, 'MATITAS', 44001),
(44001012, 'MONGUÍ', 44001),
(44001016, 'TOMARRAZÓN', 44001),
(44001017, 'VILLA MARTÍN', 44001),
(44001018, 'LAS PALMAS', 44001),
(44001020, 'CHOLES', 44001),
(44001021, 'COMEJENES', 44001),
(44001022, 'EL ABRA', 44001),
(44001024, 'LOS MORENEROS', 44001),
(44001025, 'PELECHUA', 44001),
(44001026, 'PERICO', 44001),
(44001027, 'TIGRERA', 44001),
(44001028, 'ANAIME', 44001),
(44001029, 'BOCA DE CAMARONES', 44001),
(44001031, 'CERRILLO', 44001),
(44001033, 'EBANAL', 44001),
(44001034, 'GUADUALITO', 44001),
(44001035, 'JUAN Y MEDIO', 44001),
(44001036, 'LA ARENA', 44001),
(44001040, 'PUENTE BOMBA', 44001),
(44035000, 'ALBANIA', 44035),
(44035001, 'CUESTECITAS', 44035),
(44035002, 'HUAREUAREN', 44035),
(44035003, 'LOS REMEDIOS', 44035),
(44078000, 'BARRANCAS', 44078),
(44078001, 'CARRETALITO', 44078),
(44078003, 'LAGUNITA', 44078),
(44078005, 'OREGANAL', 44078),
(44078006, 'PAPAYAL', 44078),
(44078007, 'ROCHE', 44078),
(44078008, 'SAN PEDRO', 44078),
(44078009, 'GUAYACANAL', 44078),
(44078010, 'GUACAMAYAL', 44078),
(44078011, 'POZO HONDO', 44078),
(44078013, 'NUEVO OREGANAL', 44078),
(44078014, 'PATILLA', 44078),
(44090000, 'DIBULLA', 44090),
(44090001, 'LA PUNTA', 44090),
(44090002, 'LAS FLORES', 44090),
(44090003, 'MINGUEO', 44090),
(44090004, 'PALOMINO', 44090),
(44090014, 'RÍO ANCHO', 44090),
(44090019, 'CAMPANA', 44090),
(44098000, 'DISTRACCIÓN', 44098),
(44098001, 'BUENAVISTA', 44098),
(44098002, 'CHORRERAS', 44098),
(44098003, 'CAIMITO', 44098),
(44098004, 'DOS CAMINOS', 44098),
(44098005, 'LA DUDA', 44098),
(44098007, 'LA CEIBA', 44098),
(44098008, 'LOS HORNITOS', 44098),
(44098009, 'MADRE VIEJA', 44098),
(44098010, 'PARAÍSO', 44098),
(44098011, 'POTRERITO', 44098),
(44110000, 'EL MOLINO', 44110),
(44279000, 'FONSECA', 44279),
(44279002, 'CONEJO', 44279),
(44279005, 'EL HATICO', 44279),
(44279006, 'SITIO NUEVO', 44279),
(44279007, 'CARDONAL', 44279),
(44279008, 'BANGAÑITA', 44279),
(44279011, 'EL CONFUSO', 44279),
(44279013, 'LOS ALTOS', 44279),
(44279014, 'QUEBRACHAL', 44279),
(44279015, 'POTRERITO', 44279),
(44279016, 'GUAMACHAL', 44279),
(44378000, 'HATONUEVO', 44378),
(44378001, 'TABACO', 44378),
(44378002, 'CERRO ALTO', 44378),
(44378003, 'EL PARAÍSO', 44378),
(44378004, 'EL POZO', 44378),
(44378005, 'GUAIMARITO', 44378),
(44378006, 'GUAMACHITO', 44378),
(44378007, 'LA CRUZ', 44378),
(44378008, 'LA GLORIA', 44378),
(44378009, 'LA LOMITA', 44378),
(44420000, 'LA JAGUA DEL PILAR', 44420),
(44420001, 'EL PLAN', 44420),
(44430000, 'MAICAO', 44430),
(44430002, 'CARRAIPÍA', 44430),
(44430004, 'IPAPURE', 44430),
(44430005, 'LA PAZ', 44430),
(44430006, 'LA MAJAYURA', 44430),
(44430007, 'PARAGUACHÓN', 44430),
(44430010, 'MARAÑAMANA', 44430),
(44430011, 'LA ARENA', 44430),
(44430012, 'EL LIMONCITO', 44430),
(44430013, 'YOTOJOROY', 44430),
(44430014, 'GARRAPATERO', 44430),
(44430015, 'MAKU', 44430),
(44430016, 'SANTA CRUZ', 44430),
(44430017, 'SANTA ROSA', 44430),
(44560000, 'MANAURE', 44560),
(44560001, 'ARÉMASAHIN', 44560),
(44560002, 'MUSICHI', 44560),
(44560003, 'EL PÁJARO', 44560),
(44560005, 'SANTA ROSA', 44560),
(44560006, 'SHIRURE', 44560),
(44560007, 'MAYAPO', 44560),
(44560008, 'MANZANA', 44560),
(44560009, 'LA GLORIA', 44560),
(44560010, 'LA PAZ', 44560),
(44650000, 'SAN JUAN DEL CESAR', 44650),
(44650001, 'CAÑAVERALES', 44650),
(44650002, 'CARACOLÍ', 44650),
(44650003, 'CORRAL DE PIEDRA', 44650),
(44650004, 'EL HATICO DE LOS INDIOS', 44650),
(44650005, 'EL TABLAZO', 44650),
(44650006, 'EL TOTUMO', 44650),
(44650007, 'GUAYACANAL', 44650),
(44650008, 'LA JUNTA', 44650),
(44650009, 'LA PEÑA', 44650),
(44650010, 'LA SIERRITA', 44650),
(44650011, 'LOS HATICOS', 44650),
(44650012, 'LOS PONDORES', 44650),
(44650013, 'ZAMBRANO', 44650),
(44650014, 'CORRALEJA', 44650),
(44650015, 'LA PEÑA DE LOS INDIOS', 44650),
(44650016, 'PONDORITO', 44650),
(44650017, 'VILLA DEL RÍO', 44650),
(44650018, 'LAGUNITA', 44650),
(44650019, 'LOS POZOS', 44650),
(44650020, 'POTRERITO', 44650),
(44650021, 'CURAZAO', 44650),
(44650022, 'BOCA DEL MONTE', 44650),
(44650023, 'LOS CORDONES', 44650),
(44650024, 'EL PLACER', 44650),
(44847000, 'URIBIA', 44847),
(44847002, 'BAHÍA HONDA', 44847),
(44847003, 'CABO DE LA VELA', 44847),
(44847004, 'CARRIZAL', 44847),
(44847005, 'CASTILLETES', 44847),
(44847006, 'CASUSO', 44847),
(44847007, 'EL CARDÓN', 44847),
(44847009, 'GUIMPESI', 44847),
(44847010, 'JARARA', 44847),
(44847012, 'NAZARETH', 44847),
(44847013, 'PUERTO ESTRELLA', 44847),
(44847014, 'PUERTO LÓPEZ', 44847),
(44847015, 'RANCHO GRANDE', 44847),
(44847017, 'TAPARAJÍN', 44847),
(44847018, 'TAROA', 44847),
(44847020, 'IRRAIPA', 44847),
(44847021, 'GUARERPA', 44847),
(44847023, 'PORCHINA', 44847),
(44847024, 'TAGUAIRA', 44847),
(44847025, 'JONJONCITO', 44847),
(44855000, 'URUMITA', 44855),
(44855003, 'SIERRA MONTAÑA', 44855),
(44874000, 'VILLANUEVA', 44874);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cruge_authassignment`
--

CREATE TABLE IF NOT EXISTS `cruge_authassignment` (
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text,
  `itemname` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cruge_authassignment`
--

INSERT INTO `cruge_authassignment` (`userid`, `bizrule`, `data`, `itemname`) VALUES
(2, NULL, 'N;', 'invitados'),
(12, NULL, 'N;', 'invitados'),
(14, NULL, 'N;', 'invitados'),
(15, NULL, 'N;', 'invitados'),
(16, NULL, 'N;', 'internos_municipios'),
(16, NULL, 'N;', 'invitados'),
(17, NULL, 'N;', 'internos_municipios'),
(17, NULL, 'N;', 'invitados'),
(18, NULL, 'N;', 'internos_municipios'),
(18, NULL, 'N;', 'invitados'),
(19, NULL, 'N;', 'internos_municipios'),
(19, NULL, 'N;', 'invitados'),
(20, NULL, 'N;', 'internos_municipios'),
(20, NULL, 'N;', 'invitados'),
(21, NULL, 'N;', 'internos_municipios'),
(21, NULL, 'N;', 'invitados'),
(22, NULL, 'N;', 'internos_municipios'),
(22, NULL, 'N;', 'invitados'),
(23, NULL, 'N;', 'internos_municipios'),
(23, NULL, 'N;', 'invitados'),
(24, NULL, 'N;', 'internos_municipios'),
(24, NULL, 'N;', 'invitados'),
(25, NULL, 'N;', 'internos_municipios'),
(25, NULL, 'N;', 'invitados'),
(26, NULL, 'N;', 'internos_municipios'),
(26, NULL, 'N;', 'invitados'),
(27, NULL, 'N;', 'internos_municipios'),
(27, NULL, 'N;', 'invitados'),
(28, NULL, 'N;', 'internos_municipios'),
(28, NULL, 'N;', 'invitados'),
(29, NULL, 'N;', 'internos_municipios'),
(29, NULL, 'N;', 'invitados'),
(30, NULL, 'N;', 'internos_municipios'),
(30, NULL, 'N;', 'invitados'),
(31, NULL, 'N;', 'administrador'),
(31, NULL, 'N;', 'invitados');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cruge_authitem`
--

CREATE TABLE IF NOT EXISTS `cruge_authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cruge_authitem`
--

INSERT INTO `cruge_authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('acceso_buscar', 1, 'menu buscar', '', 'N;'),
('acceso_causas', 1, 'menu causas', '', 'N;'),
('acceso_centropoblado', 1, 'menu centro poblado', '', 'N;'),
('acceso_configuraciones_sistemas', 1, 'menu configuraciones del sistemas', '', 'N;'),
('acceso_corregimiento', 1, 'menu corregimiento', '', 'N;'),
('acceso_crear_evento_de_riesgo', 1, 'menu crear evento', '', 'N;'),
('acceso_crear_evento_de_riesgo_II', 1, 'menu crear evento 2', '', 'N;'),
('acceso_cruge', 1, 'funciones cruge', '', 'N;'),
('acceso_cuenta', 1, 'menu cuenta de usuario', '', 'N;'),
('acceso_departamento', 1, 'menu departamento', '', 'N;'),
('acceso_Evento', 1, 'menu evento', '', 'N;'),
('acceso_Invitados_inicio-sesion', 1, 'paginas para invitados', '', 'N;'),
('acceso_municipio', 1, 'menu municipio', '', 'N;'),
('acceso_usuarios', 1, 'menu usuarios', '', 'N;'),
('acceso_vulnerabilidad', 1, 'menu vulnerabilidad', '', 'N;'),
('action_basicos_admin', 0, '', NULL, 'N;'),
('action_basicos_admin2', 0, '', NULL, 'N;'),
('action_basicos_busqueda', 0, '', NULL, 'N;'),
('action_basicos_calendar', 0, '', NULL, 'N;'),
('action_basicos_calendarevents', 0, '', NULL, 'N;'),
('action_basicos_comboCorregimientos', 0, '', NULL, 'N;'),
('action_basicos_create', 0, '', NULL, 'N;'),
('action_basicos_create2', 0, '', NULL, 'N;'),
('action_basicos_delete', 0, '', NULL, 'N;'),
('action_basicos_editable', 0, '', NULL, 'N;'),
('action_basicos_export', 0, '', NULL, 'N;'),
('action_basicos_export2', 0, '', NULL, 'N;'),
('action_basicos_generarpdf', 0, '', NULL, 'N;'),
('action_basicos_import', 0, '', NULL, 'N;'),
('action_basicos_index', 0, '', NULL, 'N;'),
('action_basicos_pdf', 0, '', NULL, 'N;'),
('action_basicos_update', 0, '', NULL, 'N;'),
('action_basicos_update2', 0, '', NULL, 'N;'),
('action_basicos_view', 0, '', NULL, 'N;'),
('action_causa_admin', 0, '', NULL, 'N;'),
('action_causa_create', 0, '', NULL, 'N;'),
('action_causa_delete', 0, '', NULL, 'N;'),
('action_causa_index', 0, '', NULL, 'N;'),
('action_causa_update', 0, '', NULL, 'N;'),
('action_causa_view', 0, '', NULL, 'N;'),
('action_centropoblado_admin', 0, '', NULL, 'N;'),
('action_centropoblado_create', 0, '', NULL, 'N;'),
('action_centropoblado_delete', 0, '', NULL, 'N;'),
('action_centropoblado_index', 0, '', NULL, 'N;'),
('action_centropoblado_update', 0, '', NULL, 'N;'),
('action_centropoblado_view', 0, '', NULL, 'N;'),
('action_corregimiento_admin', 0, '', NULL, 'N;'),
('action_corregimiento_create', 0, '', NULL, 'N;'),
('action_corregimiento_delete', 0, '', NULL, 'N;'),
('action_corregimiento_index', 0, '', NULL, 'N;'),
('action_corregimiento_update', 0, '', NULL, 'N;'),
('action_corregimiento_view', 0, '', NULL, 'N;'),
('action_departamento_admin', 0, '', NULL, 'N;'),
('action_departamento_create', 0, '', NULL, 'N;'),
('action_departamento_delete', 0, '', NULL, 'N;'),
('action_departamento_index', 0, '', NULL, 'N;'),
('action_departamento_update', 0, '', NULL, 'N;'),
('action_departamento_view', 0, '', NULL, 'N;'),
('action_efectos_admin', 0, '', NULL, 'N;'),
('action_efectos_create', 0, '', NULL, 'N;'),
('action_efectos_delete', 0, '', NULL, 'N;'),
('action_efectos_index', 0, '', NULL, 'N;'),
('action_efectos_update', 0, '', NULL, 'N;'),
('action_efectos_view', 0, '', NULL, 'N;'),
('action_evento_admin', 0, '', NULL, 'N;'),
('action_evento_create', 0, '', NULL, 'N;'),
('action_evento_delete', 0, '', NULL, 'N;'),
('action_evento_index', 0, '', NULL, 'N;'),
('action_evento_update', 0, '', NULL, 'N;'),
('action_evento_view', 0, '', NULL, 'N;'),
('action_municipio_admin', 0, '', NULL, 'N;'),
('action_municipio_create', 0, '', NULL, 'N;'),
('action_municipio_delete', 0, '', NULL, 'N;'),
('action_municipio_index', 0, '', NULL, 'N;'),
('action_municipio_update', 0, '', NULL, 'N;'),
('action_municipio_view', 0, '', NULL, 'N;'),
('action_refreligion_admin', 0, '', NULL, 'N;'),
('action_refreligion_create', 0, '', NULL, 'N;'),
('action_refreligion_delete', 0, '', NULL, 'N;'),
('action_refreligion_editable', 0, '', NULL, 'N;'),
('action_refreligion_export', 0, '', NULL, 'N;'),
('action_refreligion_import', 0, '', NULL, 'N;'),
('action_refreligion_index', 0, '', NULL, 'N;'),
('action_refreligion_update', 0, '', NULL, 'N;'),
('action_refreligion_view', 0, '', NULL, 'N;'),
('action_site_captcha', 0, '', NULL, 'N;'),
('action_site_contact', 0, '', NULL, 'N;'),
('action_site_error', 0, '', NULL, 'N;'),
('action_site_index', 0, '', NULL, 'N;'),
('action_site_login', 0, '', NULL, 'N;'),
('action_site_logout', 0, '', NULL, 'N;'),
('action_site_page', 0, '', NULL, 'N;'),
('action_tbemployee_admin', 0, '', NULL, 'N;'),
('action_tbemployee_calendar', 0, '', NULL, 'N;'),
('action_tbemployee_calendarevents', 0, '', NULL, 'N;'),
('action_tbemployee_create', 0, '', NULL, 'N;'),
('action_tbemployee_delete', 0, '', NULL, 'N;'),
('action_tbemployee_editable', 0, '', NULL, 'N;'),
('action_tbemployee_export', 0, '', NULL, 'N;'),
('action_tbemployee_import', 0, '', NULL, 'N;'),
('action_tbemployee_index', 0, '', NULL, 'N;'),
('action_tbemployee_update', 0, '', NULL, 'N;'),
('action_tbemployee_view', 0, '', NULL, 'N;'),
('action_ui_ajaxrbacitemdescr', 0, '', NULL, 'N;'),
('action_ui_editprofile', 0, '', NULL, 'N;'),
('action_ui_fieldsadmincreate', 0, '', NULL, 'N;'),
('action_ui_fieldsadminlist', 0, '', NULL, 'N;'),
('action_ui_fieldsadminupdate', 0, '', NULL, 'N;'),
('action_ui_rbacajaxassignment', 0, '', NULL, 'N;'),
('action_ui_rbacajaxsetchilditem', 0, '', NULL, 'N;'),
('action_ui_rbacauthitemchilditems', 0, '', NULL, 'N;'),
('action_ui_rbacauthitemcreate', 0, '', NULL, 'N;'),
('action_ui_rbacauthitemdelete', 0, '', NULL, 'N;'),
('action_ui_rbacauthitemupdate', 0, '', NULL, 'N;'),
('action_ui_rbaclistops', 0, '', NULL, 'N;'),
('action_ui_rbaclistroles', 0, '', NULL, 'N;'),
('action_ui_rbaclisttasks', 0, '', NULL, 'N;'),
('action_ui_rbacusersassignments', 0, '', NULL, 'N;'),
('action_ui_sessionadmin', 0, '', NULL, 'N;'),
('action_ui_systemupdate', 0, '', NULL, 'N;'),
('action_ui_usermanagementadmin', 0, '', NULL, 'N;'),
('action_ui_usermanagementcreate', 0, '', NULL, 'N;'),
('action_ui_usermanagementdelete', 0, '', NULL, 'N;'),
('action_ui_usermanagementupdate', 0, '', NULL, 'N;'),
('action_ui_usersaved', 0, '', NULL, 'N;'),
('action_vulnerabilidad_admin', 0, '', NULL, 'N;'),
('action_vulnerabilidad_create', 0, '', NULL, 'N;'),
('action_vulnerabilidad_delete', 0, '', NULL, 'N;'),
('action_vulnerabilidad_index', 0, '', NULL, 'N;'),
('action_vulnerabilidad_update', 0, '', NULL, 'N;'),
('action_vulnerabilidad_view', 0, '', NULL, 'N;'),
('admin', 0, '', NULL, 'N;'),
('administrador', 2, 'adminis', '', 'N;'),
('controller_basicos', 0, '', NULL, 'N;'),
('controller_causa', 0, '', NULL, 'N;'),
('controller_centropoblado', 0, '', NULL, 'N;'),
('controller_corregimiento', 0, '', NULL, 'N;'),
('controller_departamento', 0, '', NULL, 'N;'),
('controller_efectos', 0, '', NULL, 'N;'),
('controller_evento', 0, '', NULL, 'N;'),
('controller_municipio', 0, '', NULL, 'N;'),
('controller_refreligion', 0, '', NULL, 'N;'),
('controller_site', 0, '', NULL, 'N;'),
('controller_tbemployee', 0, '', NULL, 'N;'),
('controller_vulnerabilidad', 0, '', NULL, 'N;'),
('edit-advanced-profile-features', 0, 'C:\\xampp\\htdocs\\SISGR_beta1\\protected\\modules\\cruge\\views\\ui\\usermanagementupdate.php linea 114', NULL, 'N;'),
('exportar_excel_pdf_word', 1, 'exportar_documentos', '', 'N;'),
('internos_municipios', 2, 'internos', '', 'N;'),
('invitados', 2, 'invitdos', '', 'N;');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cruge_authitemchild`
--

CREATE TABLE IF NOT EXISTS `cruge_authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cruge_authitemchild`
--

INSERT INTO `cruge_authitemchild` (`parent`, `child`) VALUES
('administrador', 'acceso_buscar'),
('internos_municipios', 'acceso_buscar'),
('invitados', 'acceso_buscar'),
('administrador', 'acceso_causas'),
('administrador', 'acceso_centropoblado'),
('administrador', 'acceso_configuraciones_sistemas'),
('administrador', 'acceso_corregimiento'),
('administrador', 'acceso_crear_evento_de_riesgo'),
('internos_municipios', 'acceso_crear_evento_de_riesgo_II'),
('administrador', 'acceso_cuenta'),
('internos_municipios', 'acceso_cuenta'),
('administrador', 'acceso_departamento'),
('administrador', 'acceso_Evento'),
('administrador', 'acceso_Invitados_inicio-sesion'),
('internos_municipios', 'acceso_Invitados_inicio-sesion'),
('invitados', 'acceso_Invitados_inicio-sesion'),
('administrador', 'acceso_municipio'),
('administrador', 'acceso_pdf'),
('administrador', 'acceso_usuarios'),
('administrador', 'acceso_vulnerabilidad'),
('acceso_crear_evento_de_riesgo', 'action_basicos_admin'),
('acceso_crear_evento_de_riesgo_II', 'action_basicos_admin2'),
('acceso_crear_evento_de_riesgo', 'action_basicos_comboCorregimientos'),
('acceso_crear_evento_de_riesgo_II', 'action_basicos_comboCorregimientos'),
('acceso_crear_evento_de_riesgo', 'action_basicos_create'),
('acceso_crear_evento_de_riesgo_II', 'action_basicos_create2'),
('acceso_crear_evento_de_riesgo', 'action_basicos_delete'),
('acceso_crear_evento_de_riesgo_II', 'action_basicos_delete'),
('exportar_excel_pdf_word', 'action_basicos_export'),
('exportar_excel_pdf_word', 'action_basicos_export2'),
('acceso_pdf', 'action_basicos_generarpdf'),
('acceso_buscar', 'action_basicos_index'),
('acceso_crear_evento_de_riesgo', 'action_basicos_index'),
('acceso_crear_evento_de_riesgo_II', 'action_basicos_index'),
('acceso_pdf', 'action_basicos_pdf'),
('exportar_excel_pdf_word', 'action_basicos_pdf'),
('acceso_crear_evento_de_riesgo', 'action_basicos_update'),
('acceso_crear_evento_de_riesgo_II', 'action_basicos_update2'),
('acceso_buscar', 'action_basicos_view'),
('acceso_crear_evento_de_riesgo', 'action_basicos_view'),
('acceso_crear_evento_de_riesgo_II', 'action_basicos_view'),
('acceso_causas', 'action_causa_admin'),
('acceso_causas', 'action_causa_create'),
('acceso_causas', 'action_causa_delete'),
('acceso_causas', 'action_causa_update'),
('acceso_causas', 'action_causa_view'),
('acceso_centropoblado', 'action_centropoblado_admin'),
('acceso_centropoblado', 'action_centropoblado_create'),
('acceso_centropoblado', 'action_centropoblado_delete'),
('acceso_centropoblado', 'action_centropoblado_index'),
('acceso_centropoblado', 'action_centropoblado_update'),
('acceso_centropoblado', 'action_centropoblado_view'),
('acceso_corregimiento', 'action_corregimiento_admin'),
('acceso_corregimiento', 'action_corregimiento_create'),
('acceso_corregimiento', 'action_corregimiento_delete'),
('acceso_corregimiento', 'action_corregimiento_index'),
('acceso_corregimiento', 'action_corregimiento_update'),
('acceso_corregimiento', 'action_corregimiento_view'),
('acceso_departamento', 'action_departamento_admin'),
('acceso_departamento', 'action_departamento_create'),
('acceso_departamento', 'action_departamento_delete'),
('acceso_departamento', 'action_departamento_index'),
('acceso_departamento', 'action_departamento_update'),
('acceso_departamento', 'action_departamento_view'),
('acceso_Evento', 'action_evento_admin'),
('acceso_Evento', 'action_evento_create'),
('acceso_Evento', 'action_evento_delete'),
('acceso_Evento', 'action_evento_index'),
('acceso_Evento', 'action_evento_update'),
('acceso_Evento', 'action_evento_view'),
('acceso_municipio', 'action_municipio_admin'),
('acceso_municipio', 'action_municipio_create'),
('acceso_municipio', 'action_municipio_delete'),
('acceso_municipio', 'action_municipio_update'),
('acceso_municipio', 'action_municipio_view'),
('acceso_Invitados_inicio-sesion', 'action_site_captcha'),
('acceso_Invitados_inicio-sesion', 'action_site_index'),
('acceso_Invitados_inicio-sesion', 'action_site_login'),
('acceso_Invitados_inicio-sesion', 'action_site_logout'),
('acceso_cruge', 'action_ui_ajaxrbacitemdescr'),
('acceso_cruge', 'action_ui_editprofile'),
('acceso_cuenta', 'action_ui_editprofile'),
('acceso_cruge', 'action_ui_fieldsadmincreate'),
('acceso_cruge', 'action_ui_fieldsadminlist'),
('acceso_cruge', 'action_ui_fieldsadminupdate'),
('acceso_cruge', 'action_ui_rbacajaxassignment'),
('acceso_cruge', 'action_ui_rbacajaxsetchilditem'),
('acceso_cruge', 'action_ui_rbacauthitemchilditems'),
('acceso_cruge', 'action_ui_rbacauthitemcreate'),
('acceso_cruge', 'action_ui_rbacauthitemdelete'),
('acceso_cruge', 'action_ui_rbacauthitemupdate'),
('acceso_cruge', 'action_ui_rbaclistops'),
('acceso_cruge', 'action_ui_rbaclistroles'),
('acceso_cruge', 'action_ui_rbaclisttasks'),
('acceso_cruge', 'action_ui_rbacusersassignments'),
('acceso_usuarios', 'action_ui_rbacusersassignments'),
('acceso_cruge', 'action_ui_sessionadmin'),
('acceso_configuraciones_sistemas', 'action_ui_systemupdate'),
('acceso_cruge', 'action_ui_systemupdate'),
('acceso_cruge', 'action_ui_usermanagementadmin'),
('acceso_usuarios', 'action_ui_usermanagementadmin'),
('acceso_cruge', 'action_ui_usermanagementcreate'),
('acceso_usuarios', 'action_ui_usermanagementcreate'),
('acceso_usuarios', 'action_ui_usermanagementdelete'),
('acceso_cruge', 'action_ui_usermanagementupdate'),
('acceso_usuarios', 'action_ui_usermanagementupdate'),
('acceso_cruge', 'action_ui_usersaved'),
('acceso_cuenta', 'action_ui_usersaved'),
('acceso_vulnerabilidad', 'action_vulnerabilidad_admin'),
('administrar_vilnerabilidades', 'action_vulnerabilidad_admin'),
('acceso_vulnerabilidad', 'action_vulnerabilidad_create'),
('crear_vulnerabilidad', 'action_vulnerabilidad_create'),
('acceso_vulnerabilidad', 'action_vulnerabilidad_delete'),
('acceso_vulnerabilidad', 'action_vulnerabilidad_index'),
('listar_vulnerabilidad', 'action_vulnerabilidad_index'),
('acceso_vulnerabilidad', 'action_vulnerabilidad_update'),
('acceso_vulnerabilidad', 'action_vulnerabilidad_view'),
('listar_vulnerabilidad', 'action_vulnerabilidad_view'),
('acceso_cruge', 'admin'),
('menu_vulnerabilidad', 'administrar_vilnerabilidades'),
('acceso_vulnerabilidad', 'controller_vulnerabilidad'),
('menu_vulnerabilidad', 'crear_vulnerabilidad'),
('acceso_cruge', 'edit-advanced-profile-features'),
('acceso_usuarios', 'edit-advanced-profile-features'),
('administrador', 'exportar_excel_pdf_word'),
('internos_municipios', 'exportar_excel_pdf_word'),
('menu_vulnerabilidad', 'listar_vulnerabilidad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cruge_field`
--

CREATE TABLE IF NOT EXISTS `cruge_field` (
`idfield` int(11) NOT NULL,
  `fieldname` varchar(20) NOT NULL,
  `longname` varchar(50) DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  `required` int(11) DEFAULT '0',
  `fieldtype` int(11) DEFAULT '0',
  `fieldsize` int(11) DEFAULT '20',
  `maxlength` int(11) DEFAULT '45',
  `showinreports` int(11) DEFAULT '0',
  `useregexp` varchar(512) DEFAULT NULL,
  `useregexpmsg` varchar(512) DEFAULT NULL,
  `predetvalue` mediumblob
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cruge_field`
--

INSERT INTO `cruge_field` (`idfield`, `fieldname`, `longname`, `position`, `required`, `fieldtype`, `fieldsize`, `maxlength`, `showinreports`, `useregexp`, `useregexpmsg`, `predetvalue`) VALUES
(1, 'nombre', 'nombre', 1, 0, 0, 40, 45, 1, '', '', ''),
(2, 'apellido', 'apellido', 2, 0, 0, 40, 45, 1, '', '', ''),
(3, 'sexo', 'sexo', 3, 0, 3, 40, 45, 1, '', '', 0x686f6d6272652c686f6d6272650d0a6d756a65722c6d756a6572),
(4, 'celular', 'celular', 4, 0, 0, 40, 45, 1, '', '', ''),
(6, 'Municipio', 'Municipio', 0, 1, 3, 20, 45, 1, '', '', 0x2c200d0a34343033352c20414c42414e49410d0a34343037382c2042415252414e4341530d0a34343039302c20444942554c4c410d0a34343039382c20444953545241434349c3934e0d0a34343131302c20454c204d4f4c494e4f0d0a34343237392c20464f4e534543410d0a34343337382c204841544f4e5545564f0d0a34343432302c204c41204a414755412044454c2050494c41520d0a34343433302c204d414943414f0d0a34343536302c204d414e415552450d0a34343030312c2052494f48414348410d0a34343635302c2053414e204a55414e2044454c2043455341520d0a34343834372c205552494249410d0a34343835352c205552554d4954410d0a34343837342c2056494c4c414e55455641);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cruge_fieldvalue`
--

CREATE TABLE IF NOT EXISTS `cruge_fieldvalue` (
`idfieldvalue` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `idfield` int(11) NOT NULL,
  `value` blob
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cruge_fieldvalue`
--

INSERT INTO `cruge_fieldvalue` (`idfieldvalue`, `iduser`, `idfield`, `value`) VALUES
(26, 11, 1, 0x6a6f7365),
(27, 11, 2, 0x73616c6173),
(28, 11, 3, 0x686f6d627265),
(29, 11, 4, 0x33303134323633303432),
(30, 11, 5, 0x6c61204775616a697261),
(31, 13, 1, 0x6a6169646572),
(32, 13, 2, 0x6d656e646f7a61),
(33, 13, 3, 0x686f6d627265),
(34, 13, 4, 0x33313232343737373133),
(35, 13, 5, 0x6c61204775616a697261),
(36, 12, 1, 0x68756d626572746f),
(37, 12, 2, 0x6d656a6961),
(38, 12, 3, 0x686f6d627265),
(39, 12, 4, 0x33303034303433303438),
(40, 12, 5, 0x6c61204775616a697261),
(41, 15, 1, 0x6665726e616e646f),
(42, 15, 2, 0x68657272657261),
(43, 15, 3, 0x686f6d627265),
(44, 15, 4, 0x3330303030303030),
(45, 15, 5, 0x6c61204775616a697261),
(46, 1, 1, 0x61646d696e),
(47, 1, 2, 0x61646d696e),
(48, 1, 3, 0x686f6d627265),
(49, 1, 4, 0x33303134323633303432),
(50, 1, 5, 0x4d616764616c656e61),
(51, 28, 1, 0x6b6174686572696e65),
(52, 28, 2, 0x70756572746f20),
(53, 28, 3, 0x6d756a6572),
(54, 28, 4, 0x33313336373239303033),
(55, 28, 5, 0x6c61204775616a697261),
(56, 25, 6, 0x3434343330),
(57, 25, 1, 0x6c756973),
(58, 25, 2, 0x616c666f6e736f),
(59, 25, 3, 0x686f6d627265),
(60, 25, 4, 0x33303033343435363839),
(61, 16, 6, 0x3434303031),
(62, 16, 1, 0x616c626572746f),
(63, 16, 2, 0x736f746f),
(64, 16, 3, 0x686f6d627265),
(65, 16, 4, 0x333231353639383734),
(66, 1, 6, ''),
(67, 31, 6, 0x3434303031),
(68, 26, 6, 0x3434353630),
(69, 26, 1, 0x6a75616e),
(70, 26, 2, 0x6f7274697a),
(71, 26, 3, 0x686f6d627265),
(72, 26, 4, 0x333030303030303030),
(73, 31, 1, ''),
(74, 31, 2, ''),
(75, 31, 3, 0x686f6d627265),
(76, 31, 4, ''),
(77, 30, 6, 0x3434383734),
(78, 30, 1, ''),
(79, 30, 2, ''),
(80, 30, 3, 0x686f6d627265),
(81, 30, 4, ''),
(82, 29, 6, 0x3434383535),
(83, 29, 1, ''),
(84, 29, 2, ''),
(85, 29, 3, 0x686f6d627265),
(86, 29, 4, ''),
(87, 28, 6, 0x3434383437),
(88, 27, 6, 0x3434363530),
(89, 27, 1, ''),
(90, 27, 2, ''),
(91, 27, 3, 0x686f6d627265),
(92, 27, 4, ''),
(93, 24, 6, 0x3434343230),
(94, 24, 1, ''),
(95, 24, 2, ''),
(96, 24, 3, 0x686f6d627265),
(97, 24, 4, ''),
(98, 23, 6, 0x3434333738),
(99, 23, 1, ''),
(100, 23, 2, ''),
(101, 23, 3, 0x686f6d627265),
(102, 23, 4, ''),
(103, 22, 6, 0x3434323739),
(104, 22, 1, ''),
(105, 22, 2, ''),
(106, 22, 3, 0x686f6d627265),
(107, 22, 4, ''),
(108, 21, 6, 0x3434313130),
(109, 21, 1, ''),
(110, 21, 2, ''),
(111, 21, 3, 0x686f6d627265),
(112, 21, 4, ''),
(113, 20, 6, 0x3434303938),
(114, 20, 1, ''),
(115, 20, 2, ''),
(116, 20, 3, 0x686f6d627265),
(117, 20, 4, ''),
(118, 19, 6, 0x3434303930),
(119, 19, 1, ''),
(120, 19, 2, ''),
(121, 19, 3, 0x686f6d627265),
(122, 19, 4, ''),
(123, 18, 6, 0x3434303738),
(124, 18, 1, ''),
(125, 18, 2, ''),
(126, 18, 3, 0x686f6d627265),
(127, 18, 4, ''),
(128, 17, 6, 0x3434303335),
(129, 17, 1, ''),
(130, 17, 2, ''),
(131, 17, 3, 0x686f6d627265),
(132, 17, 4, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cruge_session`
--

CREATE TABLE IF NOT EXISTS `cruge_session` (
`idsession` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `created` bigint(30) DEFAULT NULL,
  `expire` bigint(30) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `ipaddress` varchar(45) DEFAULT NULL,
  `usagecount` int(11) DEFAULT '0',
  `lastusage` bigint(30) DEFAULT NULL,
  `logoutdate` bigint(30) DEFAULT NULL,
  `ipaddressout` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=592 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cruge_session`
--

INSERT INTO `cruge_session` (`idsession`, `iduser`, `created`, `expire`, `status`, `ipaddress`, `usagecount`, `lastusage`, `logoutdate`, `ipaddressout`) VALUES
(0, 1, 1437495204, 1437502404, 0, '::1', 1, 1437495204, NULL, NULL),
(190, 1, 1420736552, 1420740152, 0, '::1', 1, 1420736552, 1420736749, '::1'),
(191, 12, 1420736768, 1420740368, 0, '::1', 1, 1420736768, 1420737535, '::1'),
(192, 1, 1420737544, 1420741144, 0, '::1', 1, 1420737544, 1420737567, '::1'),
(193, 1, 1420738257, 1420741857, 1, '::1', 1, 1420738257, NULL, NULL),
(194, 1, 1420762251, 1420765851, 0, '::1', 1, 1420762251, NULL, NULL),
(195, 1, 1420837727, 1420841327, 0, '::1', 2, 1420840526, NULL, NULL),
(196, 1, 1420844044, 1420847644, 0, '::1', 1, 1420844044, 1420844760, '::1'),
(197, 11, 1420844776, 1420848376, 0, '::1', 1, 1420844776, 1420844831, '::1'),
(198, 12, 1420844845, 1420848445, 0, '::1', 1, 1420844845, 1420844958, '::1'),
(199, 1, 1420844987, 1420848587, 0, '::1', 3, 1420846485, NULL, NULL),
(200, 1, 1420848611, 1420852211, 0, '::1', 1, 1420848611, NULL, NULL),
(201, 1, 1420861617, 1420865217, 0, '::1', 1, 1420861617, 1420862920, '::1'),
(202, 12, 1420862771, 1420866371, 0, '::1', 1, 1420862771, 1420863228, '::1'),
(203, 11, 1420862938, 1420866538, 0, '::1', 1, 1420862938, 1420863021, '::1'),
(204, 1, 1420863098, 1420866698, 1, '::1', 1, 1420863098, NULL, NULL),
(205, 11, 1420863339, 1420866939, 0, '::1', 1, 1420863339, 1420864695, '::1'),
(206, 12, 1420864708, 1420868308, 1, '::1', 1, 1420864708, NULL, NULL),
(207, 1, 1420911541, 1420915141, 0, '::1', 1, 1420911541, NULL, NULL),
(208, 1, 1420915931, 1420919531, 0, '::1', 1, 1420915931, NULL, NULL),
(209, 1, 1420922950, 1420926550, 1, '::1', 1, 1420922950, NULL, NULL),
(210, 11, 1420950572, 1420954172, 1, '::1', 1, 1420950572, NULL, NULL),
(211, 12, 1420988657, 1420992257, 0, '::1', 1, 1420988657, NULL, NULL),
(212, 12, 1420993830, 1420997430, 0, '::1', 1, 1420993830, NULL, NULL),
(213, 1, 1420996394, 1420999994, 0, '::1', 1, 1420996394, NULL, NULL),
(214, 1, 1421005843, 1421009443, 0, '::1', 1, 1421005843, NULL, NULL),
(215, 1, 1421010757, 1421014357, 0, '::1', 1, 1421010757, NULL, NULL),
(216, 1, 1421015318, 1421018918, 0, '::1', 1, 1421015318, NULL, NULL),
(217, 1, 1421019941, 1421023541, 0, '::1', 1, 1421019941, NULL, NULL),
(218, 1, 1421026218, 1421029818, 0, '::1', 1, 1421026218, 1421026336, '::1'),
(219, 12, 1421026348, 1421029948, 0, '::1', 1, 1421026348, 1421026445, '::1'),
(220, 11, 1421026463, 1421030063, 0, '::1', 1, 1421026463, 1421026559, '::1'),
(221, 1, 1421026569, 1421030169, 0, '::1', 1, 1421026569, NULL, NULL),
(222, 1, 1421034606, 1421038206, 0, '::1', 1, 1421034606, NULL, NULL),
(223, 1, 1421038625, 1421042225, 1, '::1', 2, 1421039876, NULL, NULL),
(224, 1, 1421070256, 1421073856, 0, '::1', 3, 1421073793, NULL, NULL),
(225, 1, 1421075067, 1421078667, 0, '::1', 2, 1421075401, NULL, NULL),
(226, 1, 1421079368, 1421082968, 0, '::1', 1, 1421079368, NULL, NULL),
(227, 1, 1421083696, 1421087296, 0, '::1', 2, 1421084413, NULL, NULL),
(228, 11, 1421087418, 1421091018, 0, '::1', 1, 1421087418, NULL, NULL),
(229, 1, 1421088836, 1421092436, 0, '::1', 3, 1421092379, NULL, NULL),
(230, 1, 1421092899, 1421096499, 0, '::1', 2, 1421093796, NULL, NULL),
(231, 1, 1421096584, 1421100184, 0, '::1', 1, 1421096584, 1421097111, '::1'),
(232, 12, 1421105777, 1421109377, 0, '::1', 1, 1421105777, NULL, NULL),
(233, 12, 1421169477, 1421173077, 1, '::1', 1, 1421169477, NULL, NULL),
(234, 12, 1421183706, 1421187306, 0, '181.52.42.229', 1, 1421183706, 1421184846, '181.52.42.229'),
(235, 1, 1421184857, 1421188457, 0, '181.52.42.229', 1, 1421184857, NULL, NULL),
(236, 15, 1421184908, 1421188508, 0, '181.52.42.229', 1, 1421184908, 1421185064, '181.52.42.229'),
(237, 15, 1421185084, 1421188684, 0, '181.71.173.90', 1, 1421185084, 1421185282, '181.71.173.90'),
(238, 12, 1421186897, 1421190497, 0, '181.52.42.229', 1, 1421186897, 1421187028, '181.52.42.229'),
(239, 12, 1421203696, 1421207296, 1, '191.79.6.104', 1, 1421203696, NULL, NULL),
(240, 1, 1421204863, 1421208463, 0, '179.14.70.248', 1, 1421204863, NULL, NULL),
(241, 1, 1421208544, 1421212144, 0, '179.14.70.248', 2, 1421209247, NULL, NULL),
(242, 12, 1421210471, 1421214071, 0, '181.52.42.229', 1, 1421210471, NULL, NULL),
(243, 16, 1421211620, 1421215220, 0, '181.52.42.229', 1, 1421211620, 1421211667, '181.52.42.229'),
(244, 1, 1421212174, 1421215774, 0, '179.14.70.248', 1, 1421212174, NULL, NULL),
(245, 12, 1421215161, 1421218761, 0, '181.52.42.229', 1, 1421215161, 1421215983, '181.52.42.229'),
(246, 1, 1421215915, 1421219515, 1, '186.113.147.58', 1, 1421215915, NULL, NULL),
(247, 15, 1421234142, 1421237742, 0, '181.71.173.90', 1, 1421234142, 1421234224, '181.71.173.90'),
(248, 29, 1421240802, 1421244402, 0, '181.71.173.90', 1, 1421240802, 1421240810, '181.71.173.90'),
(249, 15, 1421240826, 1421244426, 0, '181.71.173.90', 1, 1421240826, NULL, NULL),
(250, 29, 1421248773, 1421252373, 0, '191.102.73.143', 1, 1421248773, 1421249354, '191.102.73.143'),
(251, 15, 1421249454, 1421253054, 0, '191.102.73.143', 1, 1421249454, 1421249624, '191.102.73.143'),
(252, 15, 1421250607, 1421254207, 0, '181.52.42.229', 1, 1421250607, 1421250634, '181.52.42.229'),
(253, 1, 1421250649, 1421254249, 0, '181.52.42.229', 1, 1421250649, 1421250657, '181.52.42.229'),
(254, 11, 1421250668, 1421254268, 0, '181.52.42.229', 1, 1421250668, 1421250678, '181.52.42.229'),
(255, 1, 1421250696, 1421254296, 0, '181.52.42.229', 1, 1421250696, 1421253429, '181.52.42.229'),
(256, 30, 1421252912, 1421256512, 0, '181.71.173.90', 2, 1421253371, NULL, NULL),
(257, 15, 1421253447, 1421257047, 0, '181.52.42.229', 1, 1421253447, 1421253723, '181.52.42.229'),
(258, 1, 1421253516, 1421257116, 0, '181.52.42.229', 1, 1421253516, 1421253922, '181.52.42.229'),
(259, 15, 1421253937, 1421257537, 0, '181.52.42.229', 1, 1421253937, NULL, NULL),
(260, 1, 1421254026, 1421257626, 0, '181.52.42.229', 1, 1421254026, 1421255387, '181.52.42.229'),
(261, 21, 1421263878, 1421267478, 0, '186.97.36.122', 2, 1421265665, 1421265674, '186.97.36.122'),
(262, 15, 1421264587, 1421268187, 0, '186.97.36.122', 1, 1421264587, 1421264964, '186.97.36.122'),
(263, 21, 1421265703, 1421269303, 0, '186.97.36.122', 1, 1421265703, 1421265797, '186.97.36.122'),
(264, 20, 1421272523, 1421276123, 0, '186.179.111.225', 1, 1421272523, 1421272838, '186.179.111.225'),
(265, 15, 1421286500, 1421290100, 0, '186.97.8.163', 1, 1421286500, 1421286528, '186.97.8.163'),
(266, 12, 1421333751, 1421337351, 0, '181.52.42.229', 2, 1421333816, 1421333877, '181.52.42.229'),
(267, 1, 1421333885, 1421337485, 0, '181.52.42.229', 1, 1421333885, 1421336083, '181.52.42.229'),
(268, 17, 1421334866, 1421338466, 0, '186.86.212.233', 1, 1421334866, 1421335606, '190.147.180.53'),
(269, 23, 1421338389, 1421341989, 0, '190.90.14.19', 1, 1421338389, 1421338747, '190.90.14.19'),
(270, 23, 1421339584, 1421343184, 0, '190.90.14.19', 1, 1421339584, 1421339658, '190.90.14.19'),
(271, 18, 1421341570, 1421345170, 0, '181.205.11.101', 1, 1421341570, NULL, NULL),
(272, 1, 1421531431, 1421535031, 0, '186.119.51.236', 1, 1421531431, 1421532545, '186.119.51.236'),
(273, 16, 1421532559, 1421536159, 1, '186.119.51.236', 1, 1421532559, NULL, NULL),
(274, 1, 1421623291, 1421626891, 0, '179.14.15.29', 1, 1421623291, 1421623367, '179.14.15.29'),
(275, 1, 1421678095, 1421681695, 0, '181.205.239.68', 1, 1421678095, 1421679169, '181.205.239.68'),
(276, 1, 1421714123, 1421717723, 1, '190.0.12.106', 1, 1421714123, NULL, NULL),
(277, 1, 1421860962, 1421864562, 0, '181.52.42.229', 1, 1421860962, NULL, NULL),
(278, 1, 1421936599, 1421940199, 0, '200.21.81.34', 1, 1421936599, 1421938124, '200.21.81.34'),
(279, 1, 1421938716, 1421942316, 1, '200.21.81.34', 1, 1421938716, NULL, NULL),
(280, 1, 1421950968, 1421954568, 0, '179.14.170.166', 1, 1421950968, 1421951478, '179.14.170.166'),
(281, 12, 1422115697, 1422119297, 0, '::1', 1, 1422115697, NULL, NULL),
(282, 1, 1422122835, 1422126435, 0, '::1', 1, 1422122835, NULL, NULL),
(283, 25, 1422122946, 1422126546, 0, '::1', 1, 1422122946, 1422123502, '::1'),
(284, 15, 1422123517, 1422127117, 0, '::1', 1, 1422123517, 1422123523, '::1'),
(285, 11, 1422123537, 1422127137, 0, '::1', 1, 1422123537, 1422123555, '::1'),
(286, 12, 1422123567, 1422127167, 0, '::1', 1, 1422123567, NULL, NULL),
(287, 12, 1422131795, 1422135395, 1, '::1', 1, 1422131795, NULL, NULL),
(288, 1, 1422135193, 1422138793, 1, '::1', 1, 1422135193, NULL, NULL),
(289, 1, 1422206341, 1422209941, 0, '::1', 1, 1422206341, NULL, NULL),
(290, 26, 1422249027, 1422252627, 0, '::1', 1, 1422249027, 1422250493, '::1'),
(291, 26, 1422250599, 1422254199, 0, '::1', 1, 1422250599, 1422250662, '::1'),
(292, 26, 1422275641, 1422279241, 1, '181.52.42.229', 1, 1422275641, NULL, NULL),
(293, 1, 1422276309, 1422279909, 0, '181.52.42.229', 1, 1422276309, 1422276380, '181.52.42.229'),
(294, 12, 1422276399, 1422279999, 0, '181.52.42.229', 1, 1422276399, 1422276464, '181.52.42.229'),
(295, 1, 1422276479, 1422280079, 0, '181.52.42.229', 1, 1422276479, 1422276520, '181.52.42.229'),
(296, 15, 1422276607, 1422280207, 0, '181.52.42.229', 1, 1422276607, 1422276635, '181.52.42.229'),
(297, 1, 1422281243, 1422284843, 0, '181.69.103.64', 1, 1422281243, 1422281367, '181.69.103.64'),
(298, 1, 1422287565, 1422291165, 0, '181.69.103.64', 1, 1422287565, 1422288142, '181.69.103.64'),
(299, 1, 1422306987, 1422310587, 0, '181.52.42.229', 1, 1422306987, 1422307272, '181.52.42.229'),
(300, 1, 1422307280, 1422310880, 0, '181.52.42.229', 1, 1422307280, 1422307928, '181.52.42.229'),
(301, 15, 1422373278, 1422376878, 0, '179.14.68.130', 1, 1422373278, 1422373366, '179.14.68.130'),
(302, 27, 1422373731, 1422377331, 0, '190.65.38.250', 3, 1422375901, 1422375995, '190.65.38.250'),
(303, 1, 1422376276, 1422379876, 0, '181.52.42.229', 1, 1422376276, 1422378120, '181.52.42.229'),
(304, 22, 1422378373, 1422381973, 0, '190.66.145.240', 1, 1422378373, 1422378715, '190.66.145.240'),
(305, 15, 1422391060, 1422394660, 1, '186.116.22.208', 1, 1422391060, NULL, NULL),
(306, 15, 1422401760, 1422405360, 0, '181.71.174.28', 1, 1422401760, 1422401795, '181.71.174.28'),
(307, 15, 1422447574, 1422451174, 0, '186.116.22.208', 1, 1422447574, 1422447614, '186.116.22.208'),
(308, 19, 1422457935, 1422461535, 1, '186.1.165.26', 2, 1422457989, NULL, NULL),
(309, 1, 1422457965, 1422461565, 0, '190.242.40.171', 1, 1422457965, 1422457984, '190.242.40.171'),
(310, 1, 1422486341, 1422489941, 0, '181.52.42.229', 1, 1422486341, 1422487076, '181.52.42.229'),
(311, 16, 1422487098, 1422490698, 0, '181.52.42.229', 1, 1422487098, 1422487400, '181.52.42.229'),
(312, 1, 1422487410, 1422491010, 0, '181.52.42.229', 1, 1422487410, 1422487454, '181.52.42.229'),
(313, 15, 1422492889, 1422496489, 0, '181.70.114.114', 1, 1422492889, 1422492929, '181.70.114.114'),
(314, 1, 1422494901, 1422498501, 0, '181.52.42.229', 1, 1422494901, NULL, NULL),
(315, 12, 1422505313, 1422508913, 0, '181.52.42.229', 1, 1422505313, 1422506355, '181.52.42.229'),
(316, 1, 1422538440, 1422542040, 0, '181.52.42.229', 4, 1422541666, 1422541758, '186.116.22.208'),
(317, 12, 1422538585, 1422542185, 0, '181.52.42.229', 1, 1422538585, 1422538709, '181.52.42.229'),
(318, 12, 1422539406, 1422543006, 0, '191.79.217.53', 1, 1422539406, 1422539472, '191.79.217.53'),
(319, 12, 1422539998, 1422543598, 0, '181.52.42.229', 1, 1422539998, 1422540086, '181.52.42.229'),
(320, 1, 1422542233, 1422545833, 0, '186.116.22.208', 1, 1422542233, NULL, NULL),
(321, 16, 1422544503, 1422548103, 0, '186.1.164.6', 1, 1422544503, 1422544937, '186.1.164.6'),
(322, 1, 1422546134, 1422549734, 1, '186.116.22.208', 1, 1422546134, NULL, NULL),
(323, 1, 1422623097, 1422626697, 0, '200.21.81.34', 1, 1422623097, 1422624897, '200.21.81.34'),
(324, 16, 1422628250, 1422631850, 0, '181.52.42.229', 2, 1422630597, 1422630610, '181.52.42.229'),
(325, 1, 1422629998, 1422633598, 0, '186.116.22.208', 1, 1422629998, NULL, NULL),
(326, 12, 1422630011, 1422633611, 0, '181.52.42.229', 1, 1422630011, 1422630487, '181.52.42.229'),
(327, 1, 1422647805, 1422651405, 0, '186.116.22.208', 1, 1422647805, 1422647851, '186.116.22.208'),
(328, 1, 1422648183, 1422651783, 1, '186.116.22.208', 1, 1422648183, NULL, NULL),
(329, 1, 1422664567, 1422668167, 0, '181.52.42.229', 1, 1422664567, 1422666670, '181.52.42.229'),
(330, 16, 1422666683, 1422670283, 0, '181.52.42.229', 1, 1422666683, 1422666716, '181.52.42.229'),
(331, 28, 1422668036, 1422671636, 0, '181.52.42.229', 1, 1422668036, 1422668067, '181.52.42.229'),
(332, 12, 1422668085, 1422671685, 0, '181.52.42.229', 1, 1422668085, 1422668138, '181.52.42.229'),
(333, 1, 1422737314, 1422740914, 0, '186.116.22.208', 1, 1422737314, 1422737377, '186.116.22.208'),
(334, 1, 1422738664, 1422742264, 0, '186.116.22.208', 1, 1422738664, 1422738732, '186.116.22.208'),
(335, 15, 1422801135, 1422804735, 0, '186.97.193.210', 1, 1422801135, 1422801237, '186.97.193.210'),
(336, 1, 1422810168, 1422813768, 1, '186.97.193.210', 1, 1422810168, NULL, NULL),
(337, 1, 1422828506, 1422832106, 0, '186.116.22.208', 1, 1422828506, 1422828513, '186.116.22.208'),
(338, 1, 1422829352, 1422832952, 0, '186.116.22.208', 1, 1422829352, NULL, NULL),
(339, 1, 1422891125, 1422894725, 0, '186.116.22.208', 1, 1422891125, 1422891352, '186.116.22.208'),
(340, 1, 1422891377, 1422894977, 1, '186.116.22.208', 1, 1422891377, NULL, NULL),
(341, 1, 1422986505, 1422990105, 0, '181.52.42.229', 1, 1422986505, 1422986513, '181.52.42.229'),
(342, 1, 1422986688, 1422990288, 0, '181.52.42.229', 1, 1422986688, 1422986696, '181.52.42.229'),
(343, 15, 1422991089, 1422994689, 0, '179.14.86.189', 1, 1422991089, 1422991111, '179.14.86.189'),
(344, 15, 1422991128, 1422994728, 1, '179.14.86.189', 1, 1422991128, NULL, NULL),
(345, 16, 1423084751, 1423088351, 1, '186.1.164.6', 1, 1423084751, NULL, NULL),
(346, 15, 1423102944, 1423106544, 0, '181.205.237.238', 1, 1423102944, 1423103761, '181.205.237.238'),
(347, 26, 1423148857, 1423152457, 1, '190.90.14.19', 1, 1423148857, NULL, NULL),
(348, 28, 1423152575, 1423156175, 1, '190.127.233.139', 1, 1423152575, NULL, NULL),
(349, 28, 1423164544, 1423168144, 0, '200.122.226.234', 1, 1423164544, NULL, NULL),
(350, 28, 1423170896, 1423174496, 1, '200.122.226.234', 1, 1423170896, NULL, NULL),
(351, 28, 1423233166, 1423236766, 1, '200.122.226.234', 1, 1423233166, NULL, NULL),
(352, 28, 1423249120, 1423252720, 1, '200.122.226.234', 1, 1423249120, NULL, NULL),
(353, 25, 1423254853, 1423258453, 1, '152.204.183.0', 1, 1423254853, NULL, NULL),
(354, 25, 1423276814, 1423280414, 0, '186.116.22.208', 1, 1423276814, 1423276853, '186.116.22.208'),
(355, 15, 1423511853, 1423515453, 1, '200.21.81.34', 1, 1423511853, NULL, NULL),
(356, 24, 1423686833, 1423690433, 1, '191.102.73.184', 1, 1423686833, NULL, NULL),
(357, 15, 1423702630, 1423706230, 0, '186.97.132.87', 1, 1423702630, 1423702823, '186.97.132.87'),
(358, 15, 1424447583, 1424451183, 1, '181.205.41.165', 1, 1424447583, NULL, NULL),
(359, 1, 1424793112, 1424796712, 1, '190.67.246.138', 1, 1424793112, NULL, NULL),
(360, 15, 1424808767, 1424812367, 0, '181.71.148.246', 1, 1424808767, 1424809465, '181.71.148.246'),
(361, 15, 1424809557, 1424813157, 1, '181.71.148.246', 1, 1424809557, NULL, NULL),
(362, 19, 1424811239, 1424814839, 1, '186.1.165.26', 1, 1424811239, NULL, NULL),
(363, 15, 1425080015, 1425083615, 0, '181.70.10.115', 1, 1425080015, 1425080140, '181.70.10.115'),
(364, 1, 1425936049, 1425939649, 0, '190.67.226.142', 1, 1425936049, NULL, NULL),
(365, 12, 1426082915, 1426086515, 0, '181.52.42.229', 1, 1426082915, 1426082966, '181.52.42.229'),
(366, 12, 1426083025, 1426086625, 0, '181.52.42.229', 1, 1426083025, 1426083171, '181.52.42.229'),
(367, 31, 1426083187, 1426086787, 0, '181.52.42.229', 1, 1426083187, 1426083215, '181.52.42.229'),
(368, 12, 1426083226, 1426086826, 0, '181.52.42.229', 1, 1426083226, 1426083234, '181.52.42.229'),
(369, 31, 1426083251, 1426086851, 0, '181.52.42.229', 1, 1426083251, 1426083305, '181.52.42.229'),
(370, 1, 1426083548, 1426087148, 0, '181.52.42.229', 1, 1426083548, 1426084023, '181.52.42.229'),
(371, 31, 1426083644, 1426087244, 0, '181.52.42.229', 1, 1426083644, 1426083818, '181.52.42.229'),
(372, 15, 1426174149, 1426177749, 0, '181.71.208.32', 1, 1426174149, NULL, NULL),
(373, 31, 1426196480, 1426200080, 0, '179.14.150.230', 1, 1426196480, 1426196558, '179.14.150.230'),
(374, 12, 1426198548, 1426202148, 0, '181.52.42.229', 1, 1426198548, 1426198595, '181.52.42.229'),
(375, 1, 1426337768, 1426341368, 1, '200.71.167.130', 1, 1426337768, NULL, NULL),
(376, 1, 1426463533, 1426467133, 0, '186.112.207.51', 1, 1426463533, 1426463575, '186.112.207.51'),
(377, 21, 1426602438, 1426606038, 0, '191.102.72.49', 1, 1426602438, 1426602528, '191.102.72.49'),
(378, 21, 1426603061, 1426606661, 0, '191.102.72.49', 1, 1426603061, 1426603074, '191.102.72.49'),
(379, 21, 1426603096, 1426606696, 1, '191.102.72.49', 1, 1426603096, NULL, NULL),
(380, 16, 1426686723, 1426690323, 1, '186.1.164.6', 1, 1426686723, NULL, NULL),
(381, 16, 1426713359, 1426716959, 1, '190.67.225.134', 1, 1426713359, NULL, NULL),
(382, 1, 1426721584, 1426725184, 1, '186.112.206.12', 1, 1426721584, NULL, NULL),
(383, 12, 1426737057, 1426740657, 0, '181.52.42.229', 1, 1426737057, 1426737176, '181.52.42.229'),
(384, 12, 1426738046, 1426741646, 0, '181.52.42.229', 1, 1426738046, 1426739271, '181.52.42.229'),
(385, 16, 1426781357, 1426784957, 1, '190.67.225.134', 1, 1426781357, NULL, NULL),
(386, 1, 1426863402, 1426867002, 1, '186.119.62.84', 1, 1426863402, NULL, NULL),
(387, 1, 1427139606, 1427143206, 1, '190.66.149.241', 1, 1427139606, NULL, NULL),
(388, 15, 1427233319, 1427236919, 0, '190.147.180.29', 1, 1427233319, NULL, NULL),
(389, 15, 1427329607, 1427333207, 0, '190.147.180.29', 1, 1427329607, NULL, NULL),
(390, 1, 1427833759, 1427837359, 1, '200.21.81.34', 1, 1427833759, NULL, NULL),
(391, 15, 1427840956, 1427844556, 1, '190.242.72.213', 1, 1427840956, NULL, NULL),
(392, 15, 1428235931, 1428239531, 0, '181.205.3.176', 1, 1428235931, 1428235984, '181.205.3.176'),
(393, 17, 1428269449, 1428273049, 0, '181.205.3.176', 1, 1428269449, 1428269469, '181.205.3.176'),
(394, 17, 1428269494, 1428273094, 1, '181.205.3.176', 1, 1428269494, NULL, NULL),
(395, 16, 1428350737, 1428354337, 1, '190.66.154.101', 1, 1428350737, NULL, NULL),
(396, 1, 1428539633, 1428543233, 0, '190.67.155.23', 1, 1428539633, 1428539734, '190.67.155.23'),
(397, 23, 1428589359, 1428592959, 1, '190.90.14.19', 1, 1428589359, NULL, NULL),
(398, 12, 1428705275, 1428708875, 0, '::1', 1, 1428705275, NULL, NULL),
(399, 12, 1428709598, 1428713198, 1, '::1', 1, 1428709598, NULL, NULL),
(400, 12, 1428771561, 1428775161, 0, '::1', 1, 1428771561, NULL, NULL),
(401, 12, 1428775298, 1428778898, 0, '::1', 1, 1428775298, NULL, NULL),
(402, 12, 1428781892, 1428785492, 0, '::1', 1, 1428781892, NULL, NULL),
(403, 12, 1428786319, 1428789919, 1, '::1', 1, 1428786319, NULL, NULL),
(404, 12, 1428865556, 1428869156, 0, '::1', 1, 1428865556, NULL, NULL),
(405, 12, 1428869819, 1428873419, 0, '::1', 1, 1428869819, NULL, NULL),
(406, 12, 1428873888, 1428877488, 0, '::1', 2, 1428874168, NULL, NULL),
(407, 12, 1428878309, 1428881909, 1, '::1', 1, 1428878309, NULL, NULL),
(408, 12, 1428894236, 1428897836, 0, '::1', 1, 1428894236, NULL, NULL),
(409, 12, 1428898935, 1428902535, 0, '181.52.42.229', 2, 1428901510, 1428901644, '181.52.42.229'),
(410, 15, 1428931206, 1428934806, 0, '181.52.42.229', 2, 1428933095, 1428933159, '181.52.42.229'),
(411, 12, 1428932993, 1428936593, 0, '181.52.42.229', 1, 1428932993, 1428933072, '181.52.42.229'),
(412, 15, 1428935096, 1428938696, 0, '181.52.42.229', 1, 1428935096, 1428935147, '181.52.42.229'),
(413, 16, 1428938884, 1428942484, 0, '186.1.164.6', 1, 1428938884, 1428940006, '186.1.164.6'),
(414, 16, 1428940043, 1428943643, 0, '186.1.164.6', 1, 1428940043, 1428940049, '186.1.164.6'),
(415, 1, 1428940838, 1428944438, 0, '200.21.81.34', 1, 1428940838, NULL, NULL),
(416, 25, 1428983357, 1428986957, 0, '181.52.42.229', 1, 1428983357, 1428984811, '181.52.42.229'),
(417, 25, 1428984858, 1428988458, 0, '181.52.42.229', 1, 1428984858, 1428985112, '181.52.42.229'),
(418, 25, 1429025742, 1429029342, 0, '181.152.40.231', 1, 1429025742, 1429026136, '181.152.40.231'),
(419, 31, 1429026155, 1429029755, 0, '181.152.40.231', 1, 1429026155, 1429026171, '181.152.40.231'),
(420, 25, 1429028140, 1429031740, 0, '190.90.22.106', 1, 1429028140, 1429029298, '190.90.22.106'),
(421, 15, 1429033233, 1429036833, 0, '181.71.151.82', 2, 1429033325, 1429033405, '181.71.151.82'),
(422, 17, 1429043711, 1429047311, 1, '190.147.180.64', 1, 1429043711, NULL, NULL),
(423, 17, 1429107063, 1429110663, 1, '190.147.180.64', 1, 1429107063, NULL, NULL),
(424, 26, 1429110930, 1429114530, 1, '190.90.14.19', 1, 1429110930, NULL, NULL),
(425, 12, 1429113022, 1429116622, 0, '181.52.42.229', 1, 1429113022, 1429113463, '181.52.42.229'),
(426, 15, 1429128316, 1429131916, 0, '181.70.119.182', 1, 1429128316, 1429128357, '181.70.119.182'),
(427, 15, 1429130631, 1429134231, 1, '181.70.119.182', 1, 1429130631, NULL, NULL),
(428, 15, 1429141460, 1429145060, 1, '186.113.144.145', 1, 1429141460, NULL, NULL),
(429, 15, 1429188510, 1429192110, 1, '186.113.144.145', 1, 1429188510, NULL, NULL),
(430, 12, 1429194068, 1429197668, 0, '181.52.42.229', 1, 1429194068, 1429195947, '181.52.42.229'),
(431, 31, 1429194205, 1429197805, 0, '181.52.42.229', 1, 1429194205, 1429195851, '181.52.42.229'),
(432, 15, 1429194371, 1429197971, 0, '186.113.144.145', 1, 1429194371, 1429195273, '186.113.144.145'),
(433, 15, 1429223439, 1429227039, 1, '186.113.144.145', 1, 1429223439, NULL, NULL),
(434, 12, 1429228746, 1429232346, 1, '181.52.42.229', 1, 1429228746, NULL, NULL),
(435, 15, 1429283185, 1429286785, 1, '190.90.87.198', 1, 1429283185, NULL, NULL),
(436, 15, 1430149967, 1430153567, 0, '190.67.255.54', 1, 1430149967, 1430150029, '190.67.255.54'),
(437, 15, 1430224585, 1430228185, 0, '190.67.255.54', 1, 1430224585, 1430224970, '190.67.255.54'),
(438, 17, 1430319542, 1430323142, 0, '190.147.180.64', 1, 1430319542, NULL, NULL),
(439, 17, 1430324312, 1430327912, 0, '191.102.117.132', 1, 1430324312, 1430324330, '191.102.117.132'),
(440, 17, 1430324536, 1430328136, 1, '191.102.117.132', 1, 1430324536, NULL, NULL),
(441, 15, 1430425824, 1430429424, 0, '190.67.254.75', 1, 1430425824, 1430426414, '190.67.254.75'),
(442, 31, 1430426402, 1430430002, 1, '200.21.81.34', 2, 1430428118, NULL, NULL),
(443, 15, 1430426423, 1430430023, 0, '181.205.175.75', 2, 1430427969, NULL, NULL),
(444, 12, 1430947841, 1430951441, 0, '::1', 1, 1430947841, 1430950265, '::1'),
(445, 25, 1430950279, 1430953879, 1, '::1', 1, 1430950279, NULL, NULL),
(446, 25, 1431018113, 1431021713, 0, '::1', 1, 1431018113, 1431020649, '::1'),
(447, 31, 1431020676, 1431024276, 0, '::1', 1, 1431020676, 1431021300, '::1'),
(448, 25, 1431098252, 1431101852, 0, '::1', 1, 1431098252, NULL, NULL),
(449, 25, 1431102414, 1431106014, 0, '::1', 1, 1431102414, 1431104811, '::1'),
(450, 31, 1431104827, 1431108427, 0, '::1', 1, 1431104827, 1431104859, '::1'),
(451, 1, 1431104999, 1431108599, 0, '::1', 1, 1431104999, 1431105067, '::1'),
(452, 25, 1431791292, 1431794892, 0, '::1', 1, 1431791292, 1431794400, '::1'),
(453, 12, 1431792379, 1431795979, 0, '::1', 1, 1431792379, 1431792754, '::1'),
(454, 1, 1431792782, 1431796382, 0, '::1', 1, 1431792782, NULL, NULL),
(455, 25, 1431794424, 1431798024, 0, '::1', 1, 1431794424, 1431794564, '::1'),
(456, 16, 1431794576, 1431798176, 0, '::1', 1, 1431794576, 1431794688, '::1'),
(457, 25, 1431794704, 1431798304, 0, '::1', 1, 1431794704, 1431794859, '::1'),
(458, 16, 1431794873, 1431798473, 0, '::1', 1, 1431794873, 1431796332, '::1'),
(459, 25, 1431796346, 1431799946, 0, '::1', 1, 1431796346, 1431796780, '::1'),
(460, 1, 1431796802, 1431800402, 0, '::1', 1, 1431796802, 1431796940, '::1'),
(461, 25, 1431796964, 1431800564, 0, '::1', 1, 1431796964, 1431798446, '::1'),
(462, 1, 1431797064, 1431800664, 0, '::1', 1, 1431797064, 1431798096, '::1'),
(463, 16, 1431798460, 1431802060, 0, '::1', 1, 1431798460, 1431799899, '::1'),
(464, 31, 1431799916, 1431803516, 0, '::1', 1, 1431799916, 1431800918, '::1'),
(465, 1, 1431800935, 1431804535, 0, '::1', 1, 1431800935, 1431801048, '::1'),
(466, 25, 1431801064, 1431804664, 0, '::1', 1, 1431801064, NULL, NULL),
(467, 1, 1431812017, 1431815617, 0, '::1', 1, 1431812017, NULL, NULL),
(468, 31, 1431812852, 1431816452, 0, '::1', 1, 1431812852, 1431813578, '::1'),
(469, 25, 1431813591, 1431817191, 0, '::1', 1, 1431813591, 1431813893, '::1'),
(470, 31, 1431813907, 1431817507, 0, '::1', 1, 1431813907, 1431814870, '::1'),
(471, 25, 1431814883, 1431818483, 0, '::1', 1, 1431814883, 1431815249, '::1'),
(472, 31, 1431815284, 1431818884, 0, '::1', 1, 1431815284, 1431815712, '::1'),
(473, 1, 1431815655, 1431819255, 0, '::1', 1, 1431815655, 1431817179, '::1'),
(474, 31, 1431816356, 1431819956, 0, '::1', 1, 1431816356, 1431817063, '::1'),
(475, 25, 1431832620, 1431836220, 1, '::1', 1, 1431832620, NULL, NULL),
(476, 25, 1431877274, 1431880874, 0, '::1', 1, 1431877274, NULL, NULL),
(477, 31, 1431878243, 1431881843, 0, '::1', 1, 1431878243, NULL, NULL),
(478, 25, 1431881398, 1431884998, 0, '::1', 1, 1431881398, NULL, NULL),
(479, 1, 1431882214, 1431885814, 0, '::1', 1, 1431882214, NULL, NULL),
(480, 16, 1431886909, 1431890509, 0, '::1', 1, 1431886909, NULL, NULL),
(481, 25, 1431899552, 1431903152, 0, '::1', 1, 1431899552, 1431902029, '::1'),
(482, 1, 1431901445, 1431905045, 0, '::1', 1, 1431901445, NULL, NULL),
(483, 25, 1431902042, 1431905642, 0, '::1', 1, 1431902042, 1431902082, '::1'),
(484, 16, 1431902095, 1431905695, 0, '::1', 1, 1431902095, 1431902176, '::1'),
(485, 26, 1431902197, 1431905797, 0, '::1', 1, 1431902197, 1431902573, '::1'),
(486, 31, 1431902587, 1431906187, 0, '::1', 1, 1431902587, 1431902652, '::1'),
(487, 29, 1431903359, 1431906959, 0, '::1', 1, 1431903359, 1431903461, '::1'),
(488, 23, 1431903478, 1431907078, 0, '::1', 1, 1431903478, NULL, NULL),
(489, 1, 1431906600, 1431910200, 0, '::1', 1, 1431906600, NULL, NULL),
(490, 25, 1431908443, 1431912043, 0, '::1', 1, 1431908443, 1431909225, '::1'),
(491, 31, 1431909241, 1431912841, 0, '::1', 1, 1431909241, 1431909529, '::1'),
(492, 25, 1431909542, 1431913142, 0, '::1', 1, 1431909542, 1431912355, '::1'),
(493, 1, 1431912184, 1431915784, 0, '::1', 1, 1431912184, NULL, NULL),
(494, 25, 1431912368, 1431915968, 0, '::1', 1, 1431912368, 1431912672, '::1'),
(495, 25, 1431912701, 1431916301, 0, '::1', 1, 1431912701, 1431915612, '::1'),
(496, 16, 1431915627, 1431919227, 0, '::1', 1, 1431915627, 1431915730, '::1'),
(497, 25, 1431915744, 1431919344, 0, '::1', 1, 1431915744, 1431915930, '::1'),
(498, 16, 1431915944, 1431919544, 0, '::1', 1, 1431915944, NULL, NULL),
(499, 31, 1431920154, 1431923754, 0, '::1', 1, 1431920154, 1431920188, '::1'),
(500, 16, 1431920201, 1431923801, 0, '::1', 1, 1431920201, 1431920610, '::1'),
(501, 31, 1431920626, 1431924226, 0, '::1', 1, 1431920626, 1431921374, '::1'),
(502, 17, 1431921393, 1431924993, 0, '::1', 1, 1431921393, 1431924076, '::1'),
(503, 1, 1431921499, 1431925099, 0, '::1', 1, 1431921499, NULL, NULL),
(504, 31, 1431924093, 1431927693, 0, '::1', 1, 1431924093, 1431924138, '::1'),
(505, 25, 1431924153, 1431927753, 0, '::1', 1, 1431924153, 1431924675, '::1'),
(506, 31, 1431924691, 1431928291, 0, '::1', 1, 1431924691, 1431924907, '::1'),
(507, 16, 1431924920, 1431928520, 0, '::1', 1, 1431924920, 1431924948, '::1'),
(508, 31, 1431924965, 1431928565, 0, '::1', 1, 1431924965, 1431925299, '::1'),
(509, 1, 1431925230, 1431928830, 1, '::1', 1, 1431925230, NULL, NULL),
(510, 25, 1431925315, 1431928915, 1, '::1', 1, 1431925315, NULL, NULL),
(511, 16, 1431971801, 1431975401, 0, '::1', 1, 1431971801, 1431971949, '::1'),
(512, 31, 1431971984, 1431975584, 0, '::1', 1, 1431971984, NULL, NULL),
(513, 25, 1431972102, 1431975702, 1, '::1', 1, 1431972102, NULL, NULL),
(514, 31, 1431975661, 1431979261, 0, '::1', 1, 1431975661, NULL, NULL),
(515, 31, 1431979652, 1431983252, 0, '::1', 1, 1431979652, NULL, NULL),
(516, 16, 1431984372, 1431987972, 0, '::1', 1, 1431984372, 1431984497, '::1'),
(517, 25, 1432058328, 1432061928, 0, '::1', 1, 1432058328, 1432059086, '::1'),
(518, 16, 1432066922, 1432070522, 0, '::1', 1, 1432066922, NULL, NULL),
(519, 26, 1432226465, 1432230065, 0, '::1', 1, 1432226465, NULL, NULL),
(520, 16, 1432230481, 1432234081, 0, '::1', 1, 1432230481, 1432230788, '::1'),
(521, 1, 1432230552, 1432234152, 0, '::1', 1, 1432230552, NULL, NULL),
(522, 31, 1432230804, 1432234404, 0, '::1', 1, 1432230804, 1432231632, '::1'),
(523, 16, 1432231645, 1432235245, 0, '::1', 1, 1432231645, 1432231986, '::1'),
(524, 16, 1432232005, 1432235605, 0, '::1', 1, 1432232005, 1432232745, '::1'),
(525, 31, 1432232763, 1432236363, 0, '::1', 1, 1432232763, NULL, NULL),
(526, 16, 1432264920, 1432268520, 0, '::1', 1, 1432264920, 1432267325, '::1'),
(527, 31, 1432267339, 1432270939, 0, '::1', 1, 1432267339, 1432270094, '::1'),
(528, 16, 1432270113, 1432273713, 0, '::1', 1, 1432270113, 1432271148, '::1'),
(529, 31, 1432308414, 1432312014, 0, '::1', 1, 1432308414, NULL, NULL),
(530, 1, 1432310981, 1432314581, 0, '::1', 1, 1432310981, NULL, NULL),
(531, 16, 1432312203, 1432319403, 0, '::1', 1, 1432312203, NULL, NULL),
(532, 16, 1432322121, 1432329321, 0, '::1', 1, 1432322121, 1432323767, '::1'),
(533, 31, 1432323653, 1432330853, 0, '::1', 2, 1432323781, NULL, NULL),
(534, 31, 1432331396, 1432338596, 1, '::1', 1, 1432331396, NULL, NULL),
(535, 31, 1432409551, 1432416751, 0, '::1', 1, 1432409551, 1432409652, '::1'),
(536, 25, 1432409665, 1432416865, 0, '::1', 1, 1432409665, 1432409761, '::1'),
(537, 1, 1432486540, 1432493740, 0, '::1', 1, 1432486540, 1432486892, '::1'),
(538, 16, 1432486902, 1432494102, 1, '::1', 1, 1432486902, NULL, NULL),
(539, 1, 1432998226, 1433005426, 1, '::1', 1, 1432998226, NULL, NULL),
(540, 1, 1434417180, 1434424380, 0, '::1', 1, 1434417180, 1434417190, '::1'),
(541, 31, 1434417204, 1434424404, 0, '::1', 2, 1434417204, 1434417219, '::1'),
(542, 16, 1434417229, 1434424429, 0, '::1', 1, 1434417229, 1434417461, '::1'),
(543, 31, 1434417472, 1434424672, 0, '::1', 1, 1434417472, 1434420387, '::1'),
(544, 16, 1434420407, 1434427607, 0, '::1', 1, 1434420407, 1434420754, '::1'),
(545, 31, 1434420766, 1434427966, 1, '::1', 1, 1434420766, NULL, NULL),
(546, 1, 1435479876, 1435487076, 1, '::1', 1, 1435479876, NULL, NULL),
(547, 1, 1437670007, 1437677207, 1, '::1', 2, 1437670007, NULL, NULL),
(548, 1, 1437681844, 1437689044, 0, '::1', 2, 1437681844, NULL, NULL),
(549, 1, 1437692515, 1437699715, 1, '::1', 1, 1437692515, NULL, NULL),
(550, 1, 1437709469, 1437716669, 0, '::1', 2, 1437712511, NULL, NULL),
(551, 1, 1437717004, 1437724204, 1, '::1', 1, 1437717004, NULL, NULL),
(552, 1, 1437741817, 1437749017, 0, '::1', 1, 1437741817, NULL, NULL),
(553, 1, 1437755899, 1437763099, 0, '::1', 2, 1437755899, NULL, NULL),
(554, 1, 1437767533, 1437774733, 1, '::1', 1, 1437767533, NULL, NULL),
(555, 1, 1437794924, 1437802124, 1, '::1', 1, 1437794924, NULL, NULL),
(556, 1, 1437848989, 1437856189, 0, '::1', 3, 1437849595, NULL, NULL),
(557, 1, 1437856306, 1437863506, 0, '::1', 2, 1437862776, NULL, NULL),
(558, 1, 1437867199, 1437874399, 1, '::1', 1, 1437867199, NULL, NULL),
(559, 1, 1437877860, 1437885060, 1, '::1', 1, 1437877860, NULL, NULL),
(560, 1, 1437939301, 1437946501, 1, '::1', 2, 1437939302, NULL, NULL),
(561, 1, 1437952669, 1437959869, 1, '::1', 1, 1437952669, NULL, NULL),
(562, 1, 1437968936, 1437976136, 0, '::1', 1, 1437968936, NULL, NULL),
(563, 1, 1437992786, 1437999986, 1, '::1', 1, 1437992786, NULL, NULL),
(564, 1, 1438003138, 1438010338, 0, '::1', 1, 1438003138, NULL, NULL),
(565, 1, 1438015549, 1438022749, 0, '::1', 1, 1438015549, 1438015879, '::1'),
(566, 1, 1438026995, 1438034195, 0, '::1', 1, 1438026995, NULL, NULL),
(567, 1, 1438036745, 1438043945, 1, '::1', 1, 1438036745, NULL, NULL),
(568, 1, 1438051163, 1438058363, 1, '::1', 1, 1438051163, NULL, NULL),
(569, 1, 1438059456, 1438066656, 1, '::1', 1, 1438059456, NULL, NULL),
(570, 1, 1438117281, 1438124481, 1, '::1', 1, 1438117281, NULL, NULL),
(571, 1, 1438143557, 1438150757, 1, '::1', 1, 1438143557, NULL, NULL),
(572, 1, 1438181673, 1438188873, 0, '::1', 2, 1438186033, NULL, NULL),
(573, 1, 1438188997, 1438196197, 0, '::1', 1, 1438188997, NULL, NULL),
(574, 1, 1438196875, 1438204075, 1, '192.168.0.253', 2, 1438197785, NULL, NULL),
(575, 1, 1438271061, 1438278261, 1, '::1', 1, 1438271061, NULL, NULL),
(576, 1, 1438285043, 1438292243, 0, '192.168.0.253', 1, 1438285043, NULL, NULL),
(577, 1, 1438292609, 1438299809, 1, '192.168.0.253', 1, 1438292609, NULL, NULL),
(578, 1, 1438352598, 1438359798, 0, '192.168.0.253', 1, 1438352598, NULL, NULL),
(579, 1, 1438359959, 1438367159, 1, '192.168.0.253', 1, 1438359959, NULL, NULL),
(580, 1, 1438372485, 1438379685, 0, '192.168.0.253', 1, 1438372485, NULL, NULL),
(581, 1, 1438379772, 1438386972, 1, '192.168.0.253', 1, 1438379772, NULL, NULL),
(582, 1, 1438458164, 1438465364, 1, '::1', 1, 1438458164, NULL, NULL),
(583, 1, 1438697953, 1438705153, 0, '192.168.0.253', 3, 1438705115, NULL, NULL),
(584, 1, 1438705784, 1438712984, 1, '192.168.0.253', 1, 1438705784, NULL, NULL),
(585, 1, 1438715518, 1438722718, 0, '192.168.0.253', 3, 1438722141, NULL, NULL),
(586, 1, 1438722797, 1438729997, 1, '192.168.0.253', 1, 1438722797, NULL, NULL),
(587, 1, 1438747198, 1438754398, 1, '::1', 1, 1438747198, NULL, NULL),
(588, 1, 1438784221, 1438791421, 0, '192.168.0.253', 2, 1438789074, NULL, NULL),
(589, 1, 1438792120, 1438799320, 1, '192.168.0.253', 1, 1438792120, NULL, NULL),
(590, 1, 1438805498, 1438812698, 0, '192.168.0.253', 1, 1438805498, NULL, NULL),
(591, 1, 1438812839, 1438820039, 1, '192.168.0.253', 1, 1438812839, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cruge_system`
--

CREATE TABLE IF NOT EXISTS `cruge_system` (
`idsystem` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `largename` varchar(45) DEFAULT NULL,
  `sessionmaxdurationmins` int(11) DEFAULT '30',
  `sessionmaxsameipconnections` int(11) DEFAULT '10',
  `sessionreusesessions` int(11) DEFAULT '1' COMMENT '1yes 0no',
  `sessionmaxsessionsperday` int(11) DEFAULT '-1',
  `sessionmaxsessionsperuser` int(11) DEFAULT '-1',
  `systemnonewsessions` int(11) DEFAULT '0' COMMENT '1yes 0no',
  `systemdown` int(11) DEFAULT '0',
  `registerusingcaptcha` int(11) DEFAULT '0',
  `registerusingterms` int(11) DEFAULT '0',
  `terms` blob,
  `registerusingactivation` int(11) DEFAULT '1',
  `defaultroleforregistration` varchar(64) DEFAULT NULL,
  `registerusingtermslabel` varchar(100) DEFAULT NULL,
  `registrationonlogin` int(11) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cruge_system`
--

INSERT INTO `cruge_system` (`idsystem`, `name`, `largename`, `sessionmaxdurationmins`, `sessionmaxsameipconnections`, `sessionreusesessions`, `sessionmaxsessionsperday`, `sessionmaxsessionsperuser`, `systemnonewsessions`, `systemdown`, `registerusingcaptcha`, `registerusingterms`, `terms`, `registerusingactivation`, `defaultroleforregistration`, `registerusingtermslabel`, `registrationonlogin`) VALUES
(1, 'default', NULL, 120, 10, 1, -1, -1, 0, 0, 0, 0, '', 0, 'invitados', '', 0),
(2, 'default', NULL, 30, 10, 1, -1, -1, 0, 0, 0, 0, NULL, 0, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cruge_user`
--

CREATE TABLE IF NOT EXISTS `cruge_user` (
`iduser` int(11) NOT NULL,
  `regdate` bigint(30) DEFAULT NULL,
  `actdate` bigint(30) DEFAULT NULL,
  `logondate` bigint(30) DEFAULT NULL,
  `username` varchar(64) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL COMMENT 'Hashed password',
  `authkey` varchar(100) DEFAULT NULL COMMENT 'llave de autentificacion',
  `state` int(11) DEFAULT '0',
  `totalsessioncounter` int(11) DEFAULT '0',
  `currentsessioncounter` int(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cruge_user`
--

INSERT INTO `cruge_user` (`iduser`, `regdate`, `actdate`, `logondate`, `username`, `email`, `password`, `authkey`, `state`, `totalsessioncounter`, `currentsessioncounter`) VALUES
(1, NULL, NULL, 1438812839, 'admin', 'admin@tucorreo.com', 'superusuario', NULL, 1, 0, 0),
(2, NULL, NULL, NULL, 'invitado', 'invitado', 'nopassword', NULL, 1, 0, 0),
(16, 1421211215, NULL, 1434420407, 'riohacha', 'riohacha@algo.com', 'riohacha', '08dbeb26144f194b6fe97a9f7eb8a2e9', 1, 0, 0),
(17, 1421211752, NULL, 1431921393, 'albania', 'albania@algo.com', 'albania', '0b32ec019727ca8b7fd83cd1bad338bb', 1, 0, 0),
(18, 1421211793, NULL, 1421341571, 'barrancas', 'barrancas@algo.com', 'barrancas', '1479b1d5e342920bef7cd270bbdc94f5', 1, 0, 0),
(19, 1421211844, NULL, 1424811239, 'dibulla', 'dibulla@algo.com', 'dibulla', 'e5ddae00f701ee0642ee0b01b80fd2fa', 1, 0, 0),
(20, 1421211882, NULL, 1421272523, 'distraccion', 'distraccion@algo.com', 'distraccion', 'c2d91cd80b4088d44e96b2b7d2660762', 1, 0, 0),
(21, 1421211944, NULL, 1426603096, 'elmolino', 'elmolino@algo.com', 'elmolino', '82ec78eaae9cf7c129a783fd867be6dc', 1, 0, 0),
(22, 1421212021, NULL, 1422378373, 'fonseca', 'fonseca@algo.com', 'fonseca', '94bbaa91c13b33dfa5d34bc011b7ae72', 1, 0, 0),
(23, 1421212056, NULL, 1431903478, 'hatonuevo', 'hatonuevo@algo.com', 'hatonuevo', '0c9b281e43d4acf9acd4c9e14b00f091', 1, 0, 0),
(24, 1421212125, NULL, 1423686833, 'lajaguadelpilar', 'lajaguadelpilar@algo.com', 'lajaguadelpilar', '4208fc0adced46de159f2e5d9c59296b', 1, 0, 0),
(25, 1421212171, NULL, 1432409665, 'maicao', 'maicao@algo.com', 'maicao', '4114eb762a4dd0095d069f5fee97a689', 1, 0, 0),
(26, 1421212211, NULL, 1432226465, 'manaure', 'manaure@algo.com', 'manaure', '6fc8b72f68aa3db21b9569fa9bd6acd5', 1, 0, 0),
(27, 1421212381, NULL, 1422375901, 'sanjuandelcesar', 'sanjuandelcesar@algo.com', 'sanjuandelcesar', 'a5f68d1d0dd3f5638cc7267a757cf909', 1, 0, 0),
(28, 1421212420, NULL, 1423249121, 'uribia', 'uribia@algo.com', 'f7035f30', '484b995e9a680a617a3b4b8e07b86d03', 1, 0, 0),
(29, 1421212467, NULL, 1431903359, 'urumita', 'urumita@algo.com', 'urumita', 'a2376a98c1e45ede527f5b98a877faeb', 1, 0, 0),
(30, 1421212585, NULL, 1421253371, 'villanueva', 'villanueva@algo.com', 'villanueva', 'ef8e54a954af8ada3e201c21ff738b92', 1, 0, 0),
(31, 1426083099, NULL, 1434420766, 'corpoguajira', 'corpoguajira@algo.com.co', 'corpoguajira', 'b36e596697ac2cbd240fe5b64e7888ea', 1, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenca`
--

CREATE TABLE IF NOT EXISTS `cuenca` (
`id` int(11) NOT NULL,
  `codigo` int(11) NOT NULL,
  `cuenca` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE IF NOT EXISTS `departamento` (
  `id` int(11) NOT NULL,
  `departamento` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`id`, `departamento`) VALUES
(5, 'ANTIOQUIA'),
(8, 'ATLANTICO'),
(13, 'BOLIVAR'),
(15, 'BOYACA'),
(41, 'HUILA'),
(44, 'LA GUAJIRA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `financiacion`
--

CREATE TABLE IF NOT EXISTS `financiacion` (
  `id` int(11) NOT NULL,
  `financiacion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `financiacion`
--

INSERT INTO `financiacion` (`id`, `financiacion`) VALUES
(9, 'Sistema General de Regalia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formulador`
--

CREATE TABLE IF NOT EXISTS `formulador` (
`id` int(11) NOT NULL,
  `idtlusuario` int(11) NOT NULL,
  `tipodocumento` varchar(30) NOT NULL,
  `documento` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `formulador`
--

INSERT INTO `formulador` (`id`, `idtlusuario`, `tipodocumento`, `documento`, `nombre`, `email`) VALUES
(1, 1, '1', 3, 'h', 'jj');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `indicador`
--

CREATE TABLE IF NOT EXISTS `indicador` (
`id` int(11) NOT NULL,
  `idact` int(11) NOT NULL,
  `idunidad` int(11) NOT NULL,
  `indicador` varchar(250) NOT NULL,
  `lineabase` int(11) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `indicador`
--

INSERT INTO `indicador` (`id`, `idact`, `idunidad`, `indicador`, `lineabase`, `total`) VALUES
(1, 1, 61, 'Subzonas hidrográficas con planes de ordenación y manejo - POMCA - formulados.', 0, 3),
(2, 1, 61, 'Ajustes de POMCAS según Decreto 1640-2012', 4, 4),
(3, 1, 61, 'Subzonas hidrográficas con planes de ordenación y manejo - POMCA - en ejecución.', 0, 5),
(4, 1, 2, 'Cuencas con plan de ordenación debidamente adoptado / Total de Cuencas priorizadas en la jurisdicción.', 0, 100),
(5, 2, 61, 'Municipios asesorados en revisión y ajuste de los POT.', 2, 13),
(6, 3, 61, 'Municipios asesorados en formulación del SIGAM y SISBIM.', 1, 14),
(7, 4, 61, 'POT con seguimiento y asesorías realizadas a los POT de los municipios.', 15, 15),
(8, 5, 61, 'Mapas temáticos elaborados', 0, 30),
(9, 6, 2, 'Proyectos georeferenciados con relación al total de proyectos ejecutados.', 0, 100),
(10, 7, 61, 'Municipios asesorados en la incorporación del componente ambiental ecosistemas marino-costeros.', 0, 4),
(11, 8, 61, 'Observatorio ambiental diseñado e implementado.', 0, 1),
(12, 9, 61, 'Estudio de delimitación y zonificación en paramos y humedales a escala 1:25000', 0, 9),
(13, 10, 2, 'Módulos operando en la Corporación / Total de módulos del SÍA.', 43, 100),
(14, 11, 61, 'Consejo departamentales y municipales acompañados en Gestión del Riesgo de Desastres.', 0, 16),
(15, 12, 22, 'Hectáreas de deforestación evitadas.', 0, 2500),
(16, 13, 61, 'Proyecto de captura de carbono desarrollado.', 1, 2),
(17, 14, 61, 'Instalación de estaciones hidrometeorológicas automáticas en el sur de La Guajira.', 4, 2),
(18, 14, 61, 'Estaciones medidoras de nivel de rió en el sur del departamento.', 4, 2),
(19, 14, 61, 'Estaciones meteorológicas análogas para registro histórico de temperatura y precipitación en el sur de La Guajira.', 10, 2),
(20, 14, 61, 'Pronóstico y monitoreo diario de las condiciones hidrometeorológicas en el departamento de La Guajira.', 1000, 1095),
(21, 14, 24, 'Población beneficiada por sistema de alerta temprana en deslizamientos e inundaciones.', 10000, 20000),
(22, 15, 61, 'Numero de boletines de monitoreo al Sistema de Alerta Temprana y su difusión.', 60, 60),
(23, 16, 61, 'Municipios costeros evaluados en riesgo ecológico.', 0, 4),
(24, 17, 61, 'Estudios realizados de acuerdo a la zonificación establecidas.', 1, 2),
(25, 18, 61, 'Determinantes ambientales actualizadas.', 1, 1),
(26, 18, 61, 'Número de municipios con incluisión del riesgo en sus POT a partir de las determinantes ambientales generadas por la Corporación.', 15, 15),
(27, 19, 61, 'Municipios y sectores productivos/comunidad asesoradas en incorporación de políticas de adaptación al cambio climático.', 0, 20),
(28, 20, 57, 'M³ de obras de control de inundaciones, erosión, caudales, escorrentía, rectificación y manejo de cauces, obras de geotécnia y demás obras para el manejo de aguas.', 82000, 1500);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lestrategica`
--

CREATE TABLE IF NOT EXISTS `lestrategica` (
`id` int(11) NOT NULL,
  `idpgar` int(11) NOT NULL,
  `lineaestrategica` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `lestrategica`
--

INSERT INTO `lestrategica` (`id`, `idpgar`, `lineaestrategica`) VALUES
(1, 1, 'Gestion integral de los Recursos Naturales y el Ambiente para el desarrollo sostenible.'),
(2, 1, 'Recuperar y mantener los Ecosistemas Estratégicos.'),
(3, 1, 'Planificación Ambiental para la Orientacion de la Sociedad Hacia la Eficiente Ocupacion del Territorio. '),
(4, 1, 'Participación para el desarrollo de una cultura ambiental mas amigable con nuestro entorno.'),
(5, 1, 'Producción y democratizacion del conocimiento como apoyo a la gestión ambiental territorial.'),
(6, 1, 'Corpoguajira como entidad líder y articuladora de la gestión ambiental');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lexpc`
--

CREATE TABLE IF NOT EXISTS `lexpc` (
`id` int(11) NOT NULL,
  `idle` int(11) NOT NULL,
  `idpc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localizacion`
--

CREATE TABLE IF NOT EXISTS `localizacion` (
`id` int(11) NOT NULL,
  `codigo` int(11) NOT NULL,
  `localizacion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localizaciones`
--

CREATE TABLE IF NOT EXISTS `localizaciones` (
`id` int(11) NOT NULL,
  `idsubproy` int(11) NOT NULL,
  `idmun` int(11) NOT NULL,
  `idcorr` int(11) NOT NULL,
  `idcuenca` int(11) NOT NULL,
  `idsubcuenca` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `meta`
--

CREATE TABLE IF NOT EXISTS `meta` (
`id` int(11) NOT NULL,
  `idind` int(11) NOT NULL,
  `anio` year(4) NOT NULL,
  `meta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipio`
--

CREATE TABLE IF NOT EXISTS `municipio` (
  `id` int(11) NOT NULL,
  `municipio` varchar(30) NOT NULL,
  `iddep` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `municipio`
--

INSERT INTO `municipio` (`id`, `municipio`, `iddep`) VALUES
(44001, 'RIOHACHA', 44),
(44035, 'ALBANIA', 44),
(44078, 'BARRANCAS', 44),
(44090, 'DIBULLA', 44),
(44098, 'DISTRACCIÓN', 44),
(44110, 'EL MOLINO', 44),
(44279, 'FONSECA', 44),
(44378, 'HATONUEVO', 44),
(44420, 'LA JAGUA DEL PILAR', 44),
(44430, 'MAICAO', 44),
(44560, 'MANAURE', 44),
(44650, 'SAN JUAN DEL CESAR', 44),
(44847, 'URIBIA', 44),
(44855, 'URUMITA', 44),
(44874, 'VILLANUEVA', 44);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `objespecificos`
--

CREATE TABLE IF NOT EXISTS `objespecificos` (
`idobjespecifico` int(11) NOT NULL,
  `nomobjespecifico` varchar(45) DEFAULT NULL,
  `producto` varchar(45) DEFAULT NULL,
  `idproyecto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='	';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pa`
--

CREATE TABLE IF NOT EXISTS `pa` (
`id` int(11) NOT NULL,
  `idpgar` int(11) NOT NULL,
  `anioinicio` year(4) NOT NULL,
  `aniofinal` year(4) NOT NULL,
  `planaccion` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pa`
--

INSERT INTO `pa` (`id`, `idpgar`, `anioinicio`, `aniofinal`, `planaccion`) VALUES
(1, 1, 2012, 2015, '2012-2015');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pgar`
--

CREATE TABLE IF NOT EXISTS `pgar` (
`id` int(11) NOT NULL,
  `fecha` year(4) NOT NULL,
  `fechafinal` year(4) NOT NULL,
  `detalle` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pgar`
--

INSERT INTO `pgar` (`id`, `fecha`, `fechafinal`, `detalle`) VALUES
(1, 2009, 2019, '2009-2019');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programacorporactivo`
--

CREATE TABLE IF NOT EXISTS `programacorporactivo` (
`id` int(11) NOT NULL,
  `codigo` varchar(20) NOT NULL,
  `programacorporactivo` varchar(500) NOT NULL,
  `idpa` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `programacorporactivo`
--

INSERT INTO `programacorporactivo` (`id`, `codigo`, `programacorporactivo`, `idpa`) VALUES
(1, '1', 'Planificación, Ordenamiento Ambiental y Territorial', 1),
(2, '2', 'Gestión Integral del Recurso Hidrico', 1),
(3, '3', 'Bosques, Biodiversidad y Servicios Ecosistemicos ', 1),
(4, '4', 'Gestión Ambiental Sectorial y Urbana', 1),
(5, '5', 'Educación Ambiental', 1),
(6, '6', 'Calidad Ambiental', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE IF NOT EXISTS `proyecto` (
`id` int(11) NOT NULL,
  `idpc` int(11) NOT NULL,
  `codproy` varchar(20) NOT NULL,
  `proyecto` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proyecto`
--

INSERT INTO `proyecto` (`id`, `idpc`, `codproy`, `proyecto`) VALUES
(3, 1, '11', 'Planificación, Ordenamiento e Información Ambiental Territorial'),
(7, 1, '12', 'Gestión del Riesgo y adaptación al Cambio Climático.'),
(8, 1, '13', 'Gestión del conocimiento y Cooperación Internacional. '),
(9, 2, '21', 'Administración de la oferta y demanda del recurso hidrico. (superficiales y subterráneas).'),
(10, 2, '25', 'Monitoreo de la calidad del recurso hidrico'),
(11, 3, '36', 'Ecosistemas estratégicos continentales y marinos costeros'),
(12, 3, '37', 'Protección y conservación de la biodiversidad'),
(13, 3, '38', 'Negocios verdes y sostenibles'),
(14, 4, '49', 'Gestión Ambiental Urbana'),
(15, 4, '410', 'Gestión Ambiental Sectorial'),
(16, 4, '411', 'Calidad del aire'),
(17, 5, '512', 'Cultura Ambiental'),
(18, 5, '513', 'Participación Comunitaria'),
(19, 6, '614', 'Monitoreo y evaluación de la calidad de los recursos naturales y la biodiversidad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `responsable`
--

CREATE TABLE IF NOT EXISTS `responsable` (
  `id` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `idarea` int(11) NOT NULL,
  `idproyecto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcuenca`
--

CREATE TABLE IF NOT EXISTS `subcuenca` (
`id` int(11) NOT NULL,
  `codigo` int(11) NOT NULL,
  `subcuenca` varchar(50) NOT NULL,
  `idcuenca` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subproyecto`
--

CREATE TABLE IF NOT EXISTS `subproyecto` (
`id` int(11) NOT NULL,
  `codigo` int(11) NOT NULL,
  `fechaderegistro` date NOT NULL,
  `radicado` int(11) NOT NULL,
  `fecharadicado` date NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `detalle` varchar(500) NOT NULL,
  `objetivogeneral` varchar(200) NOT NULL,
  `idlocalizacion` int(11) NOT NULL,
  `poblacionben` int(11) NOT NULL,
  `idunidad` int(11) NOT NULL,
  `meta` int(11) NOT NULL,
  `idrecursos` int(11) NOT NULL,
  `idpa` int(11) NOT NULL,
  `idpc` int(11) NOT NULL,
  `idproy` int(11) NOT NULL,
  `idact` int(11) NOT NULL,
  `idformulador` int(11) NOT NULL,
  `valortotal` double NOT NULL,
  `valorsolicitado` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblusuario`
--

CREATE TABLE IF NOT EXISTS `tblusuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `sexo` varchar(15) NOT NULL,
  `celular` int(11) NOT NULL,
  `direccion` varchar(30) NOT NULL,
  `cargo` varchar(40) NOT NULL,
  `email` varchar(60) NOT NULL,
  `iddep` int(11) NOT NULL,
  `idmun` int(11) NOT NULL,
  `foto` blob NOT NULL,
  `estado` varchar(10) NOT NULL,
  `cruge_user_iduser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidaddemedida`
--

CREATE TABLE IF NOT EXISTS `unidaddemedida` (
`id` int(11) NOT NULL,
  `unidad` varchar(20) NOT NULL,
  `simbolo` varchar(23) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `unidaddemedida`
--

INSERT INTO `unidaddemedida` (`id`, `unidad`, `simbolo`) VALUES
(1, 'pesos', '$'),
(2, 'porcentaje', '%'),
(3, 'numero de viviendas', '# viviendas'),
(4, 'ADMON Costos A.I.U', 'ADMON Costos A.I.U'),
(5, 'Amperios hora', 'AH'),
(6, 'Pesos por año', '$/año'),
(7, 'Pesos por beneficiar', '$/beneficiario'),
(8, 'camas', 'camas'),
(9, 'capacidad', 'capacidad'),
(10, 'cartilla', 'cartilla'),
(11, 'numero de conexiones', 'conexiones'),
(12, 'pesos por consumo', '$/consumo'),
(13, 'contrato', 'contrato'),
(14, 'cupos', 'cupos'),
(15, 'Dia', 'Dia'),
(16, 'Equipos', 'Equipos'),
(17, 'eventos', 'eventos'),
(18, 'Familias Beneficiada', 'Familias'),
(19, 'Galon', 'gl'),
(20, 'global', 'global'),
(21, 'Pesos por hectárea ', '$/Ha'),
(22, 'Hectárea ', 'Ha'),
(23, 'Hectáreas por año', 'Ha/año'),
(24, 'Numero de Habitantes', '#Habitantes'),
(25, 'Hectáreas por munici', 'Ha/municipio'),
(26, 'hora', 'HORA'),
(27, 'Hora Maquina ', 'HM'),
(28, 'Caballos de fuerza', 'Hp'),
(29, 'Horas al dia', 'hr/dia'),
(30, 'Hombres al mes', 'Hr-mes'),
(31, 'inf. diag. por año', 'inf. diag./año'),
(32, 'informe por año', 'infor /año'),
(33, 'INTV interventoria ', 'INTV interventoria'),
(34, 'Jornal', 'Jornal'),
(35, 'Juego', 'Juego'),
(36, 'kilogramo', 'kg'),
(37, 'KIT', 'kit'),
(38, 'Pesos por kilómetro ', '$/km'),
(39, 'Kilómetros ', 'km'),
(40, 'Pesos por km por año', '$/km/año'),
(41, 'kilómetro por hora ', 'km/Hr'),
(42, 'kilómetro via', 'km-via'),
(43, 'kilómetro cuadrado', 'km2'),
(44, 'pesos por kilo volti', '$/KVA'),
(45, 'Kilo Voltio Amperio', 'KVA'),
(46, 'Kilovatio', 'KW'),
(47, 'Litros por segundo', 'lps'),
(48, 'Pesos por metro', '$/m'),
(49, 'Metros', 'm'),
(50, 'mes', 'mes'),
(51, 'Minutos', 'min'),
(52, 'Minas/año', 'Minas selec. por año'),
(53, 'Metro lineal', 'ml'),
(54, 'Mega voltio amperio', 'MVA'),
(55, 'Metro cuadrado', 'm2'),
(56, 'Pesos por metro cubi', '$/m3'),
(57, 'Metro cubico', 'm3'),
(58, 'Metro Cubico por año', 'm3/año'),
(59, 'metro cubico sobre k', 'm3/km'),
(60, 'N. Puestos Dotados', 'NPD'),
(61, 'Numero', 'Numero'),
(62, 'Numero de Barrios', 'Barrios'),
(63, 'Numero de Fincas', 'Fincas'),
(64, 'Numero de municipios', 'Municipios'),
(65, 'Numero de veredas', 'Veredas'),
(66, 'pers. capaci por año', 'pers. capaci/año'),
(67, 'Personas', 'personas'),
(68, 'Pesos por habitante', '$/habitante'),
(69, 'Planes mejor por año', 'Planes mejor/año'),
(70, 'Proyectos', 'Proyectos'),
(71, 'Punto', 'Punto'),
(72, 'Rollo', 'Rollo'),
(73, 'Suscriptores', 'Suscriptores'),
(74, 'Talleres', 'Talleres'),
(75, 'Tonelada-Metro', 'T-M'),
(76, 'Tonelada', 'Ton'),
(77, 'Toneladas al año', 'Toneladas/año'),
(78, 'Unidad', 'und'),
(79, 'usuarios', 'usuarios'),
(80, 'N. vehículos por dia', 'vehículos/dia'),
(81, 'Viaje', 'Viaje'),
(82, 'Pesos por vivienda', '$/vivienda'),
(83, 'Vatios pico', 'Wp');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valorpa`
--

CREATE TABLE IF NOT EXISTS `valorpa` (
`id` int(11) NOT NULL,
  `idproy` int(11) NOT NULL,
  `anio` year(4) NOT NULL,
  `valor` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valorsubproy`
--

CREATE TABLE IF NOT EXISTS `valorsubproy` (
`id` int(11) NOT NULL,
  `idrecurso` int(11) NOT NULL,
  `Aportante` varchar(100) DEFAULT NULL,
  `idsub` int(11) NOT NULL,
  `valor` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zonificacion`
--

CREATE TABLE IF NOT EXISTS `zonificacion` (
`id` int(11) NOT NULL,
  `idmun` int(11) NOT NULL,
  `idcorre` int(11) NOT NULL,
  `idcuenca` int(11) NOT NULL,
  `idsubcuenca` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividada`
--
ALTER TABLE `actividada`
 ADD PRIMARY KEY (`id`), ADD KEY `idpro` (`idpro`);

--
-- Indices de la tabla `anexo`
--
ALTER TABLE `anexo`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `area`
--
ALTER TABLE `area`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `corregimiento`
--
ALTER TABLE `corregimiento`
 ADD PRIMARY KEY (`id`), ADD KEY `idmun` (`idmun`);

--
-- Indices de la tabla `cruge_authassignment`
--
ALTER TABLE `cruge_authassignment`
 ADD PRIMARY KEY (`userid`,`itemname`), ADD KEY `fk_cruge_authassignment_cruge_authitem1` (`itemname`), ADD KEY `fk_cruge_authassignment_user` (`userid`);

--
-- Indices de la tabla `cruge_authitem`
--
ALTER TABLE `cruge_authitem`
 ADD PRIMARY KEY (`name`);

--
-- Indices de la tabla `cruge_authitemchild`
--
ALTER TABLE `cruge_authitemchild`
 ADD PRIMARY KEY (`parent`,`child`), ADD KEY `child` (`child`);

--
-- Indices de la tabla `cruge_field`
--
ALTER TABLE `cruge_field`
 ADD PRIMARY KEY (`idfield`);

--
-- Indices de la tabla `cruge_fieldvalue`
--
ALTER TABLE `cruge_fieldvalue`
 ADD PRIMARY KEY (`idfieldvalue`), ADD KEY `fk_cruge_fieldvalue_cruge_user1` (`iduser`), ADD KEY `fk_cruge_fieldvalue_cruge_field1` (`idfield`);

--
-- Indices de la tabla `cruge_session`
--
ALTER TABLE `cruge_session`
 ADD PRIMARY KEY (`idsession`), ADD KEY `crugesession_iduser` (`iduser`);

--
-- Indices de la tabla `cruge_system`
--
ALTER TABLE `cruge_system`
 ADD PRIMARY KEY (`idsystem`);

--
-- Indices de la tabla `cruge_user`
--
ALTER TABLE `cruge_user`
 ADD PRIMARY KEY (`iduser`);

--
-- Indices de la tabla `cuenca`
--
ALTER TABLE `cuenca`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `financiacion`
--
ALTER TABLE `financiacion`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `formulador`
--
ALTER TABLE `formulador`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `indicador`
--
ALTER TABLE `indicador`
 ADD PRIMARY KEY (`id`), ADD KEY `idact` (`idact`), ADD KEY `idunidad` (`idunidad`);

--
-- Indices de la tabla `lestrategica`
--
ALTER TABLE `lestrategica`
 ADD PRIMARY KEY (`id`), ADD KEY `idpgar` (`idpgar`);

--
-- Indices de la tabla `lexpc`
--
ALTER TABLE `lexpc`
 ADD PRIMARY KEY (`id`), ADD KEY `idle` (`idle`), ADD KEY `idpc` (`idpc`);

--
-- Indices de la tabla `localizacion`
--
ALTER TABLE `localizacion`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `localizaciones`
--
ALTER TABLE `localizaciones`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `meta`
--
ALTER TABLE `meta`
 ADD PRIMARY KEY (`id`), ADD KEY `idind` (`idind`);

--
-- Indices de la tabla `municipio`
--
ALTER TABLE `municipio`
 ADD PRIMARY KEY (`id`), ADD KEY `iddep` (`iddep`);

--
-- Indices de la tabla `objespecificos`
--
ALTER TABLE `objespecificos`
 ADD PRIMARY KEY (`idobjespecifico`), ADD KEY `idproyecto` (`idproyecto`);

--
-- Indices de la tabla `pa`
--
ALTER TABLE `pa`
 ADD PRIMARY KEY (`id`), ADD KEY `idpgar` (`idpgar`);

--
-- Indices de la tabla `pgar`
--
ALTER TABLE `pgar`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `programacorporactivo`
--
ALTER TABLE `programacorporactivo`
 ADD PRIMARY KEY (`id`), ADD KEY `idpa` (`idpa`);

--
-- Indices de la tabla `proyecto`
--
ALTER TABLE `proyecto`
 ADD PRIMARY KEY (`id`), ADD KEY `idpc` (`idpc`);

--
-- Indices de la tabla `responsable`
--
ALTER TABLE `responsable`
 ADD PRIMARY KEY (`id`), ADD KEY `idusuario` (`idusuario`), ADD KEY `idarea` (`idarea`), ADD KEY `idproyecto` (`idproyecto`);

--
-- Indices de la tabla `subcuenca`
--
ALTER TABLE `subcuenca`
 ADD PRIMARY KEY (`id`), ADD KEY `idcuenca` (`idcuenca`);

--
-- Indices de la tabla `subproyecto`
--
ALTER TABLE `subproyecto`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tblusuario`
--
ALTER TABLE `tblusuario`
 ADD PRIMARY KEY (`id`), ADD KEY `iddep` (`iddep`,`idmun`), ADD KEY `idmun` (`idmun`), ADD KEY `fk_tblusuario_cruge_user1_idx` (`cruge_user_iduser`);

--
-- Indices de la tabla `unidaddemedida`
--
ALTER TABLE `unidaddemedida`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `valorpa`
--
ALTER TABLE `valorpa`
 ADD PRIMARY KEY (`id`), ADD KEY `idproy` (`idproy`);

--
-- Indices de la tabla `valorsubproy`
--
ALTER TABLE `valorsubproy`
 ADD PRIMARY KEY (`id`), ADD KEY `idrecurso` (`idrecurso`), ADD KEY `idsub` (`idsub`);

--
-- Indices de la tabla `zonificacion`
--
ALTER TABLE `zonificacion`
 ADD PRIMARY KEY (`id`), ADD KEY `idmun` (`idmun`), ADD KEY `idcorre` (`idcorre`), ADD KEY `idcuenca` (`idcuenca`), ADD KEY `idsubcuenca` (`idsubcuenca`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividada`
--
ALTER TABLE `actividada`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=132;
--
-- AUTO_INCREMENT de la tabla `anexo`
--
ALTER TABLE `anexo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cruge_field`
--
ALTER TABLE `cruge_field`
MODIFY `idfield` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `cruge_fieldvalue`
--
ALTER TABLE `cruge_fieldvalue`
MODIFY `idfieldvalue` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=133;
--
-- AUTO_INCREMENT de la tabla `cruge_session`
--
ALTER TABLE `cruge_session`
MODIFY `idsession` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=592;
--
-- AUTO_INCREMENT de la tabla `cruge_system`
--
ALTER TABLE `cruge_system`
MODIFY `idsystem` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `cruge_user`
--
ALTER TABLE `cruge_user`
MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT de la tabla `cuenca`
--
ALTER TABLE `cuenca`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `formulador`
--
ALTER TABLE `formulador`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `indicador`
--
ALTER TABLE `indicador`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT de la tabla `lestrategica`
--
ALTER TABLE `lestrategica`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `lexpc`
--
ALTER TABLE `lexpc`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `localizacion`
--
ALTER TABLE `localizacion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `localizaciones`
--
ALTER TABLE `localizaciones`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `meta`
--
ALTER TABLE `meta`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `objespecificos`
--
ALTER TABLE `objespecificos`
MODIFY `idobjespecifico` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pa`
--
ALTER TABLE `pa`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `pgar`
--
ALTER TABLE `pgar`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `programacorporactivo`
--
ALTER TABLE `programacorporactivo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `proyecto`
--
ALTER TABLE `proyecto`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `subcuenca`
--
ALTER TABLE `subcuenca`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `subproyecto`
--
ALTER TABLE `subproyecto`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `unidaddemedida`
--
ALTER TABLE `unidaddemedida`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT de la tabla `valorpa`
--
ALTER TABLE `valorpa`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `valorsubproy`
--
ALTER TABLE `valorsubproy`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `zonificacion`
--
ALTER TABLE `zonificacion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actividada`
--
ALTER TABLE `actividada`
ADD CONSTRAINT `proyact23` FOREIGN KEY (`idpro`) REFERENCES `proyecto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `indicador`
--
ALTER TABLE `indicador`
ADD CONSTRAINT `actind4` FOREIGN KEY (`idact`) REFERENCES `actividada` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `umedida` FOREIGN KEY (`idunidad`) REFERENCES `unidaddemedida` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `lestrategica`
--
ALTER TABLE `lestrategica`
ADD CONSTRAINT `rel01` FOREIGN KEY (`idpgar`) REFERENCES `pgar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `lexpc`
--
ALTER TABLE `lexpc`
ADD CONSTRAINT `lest1` FOREIGN KEY (`idle`) REFERENCES `lestrategica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `pa` FOREIGN KEY (`idpc`) REFERENCES `pa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `meta`
--
ALTER TABLE `meta`
ADD CONSTRAINT `indmet` FOREIGN KEY (`idind`) REFERENCES `indicador` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `municipio`
--
ALTER TABLE `municipio`
ADD CONSTRAINT `municipio_ibfk_1` FOREIGN KEY (`iddep`) REFERENCES `departamento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `objespecificos`
--
ALTER TABLE `objespecificos`
ADD CONSTRAINT `subproyecto11` FOREIGN KEY (`idproyecto`) REFERENCES `subproyecto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `pa`
--
ALTER TABLE `pa`
ADD CONSTRAINT `pgarpa` FOREIGN KEY (`idpgar`) REFERENCES `pgar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `programacorporactivo`
--
ALTER TABLE `programacorporactivo`
ADD CONSTRAINT `pa1` FOREIGN KEY (`idpa`) REFERENCES `pa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `proyecto`
--
ALTER TABLE `proyecto`
ADD CONSTRAINT `proycort` FOREIGN KEY (`idpc`) REFERENCES `programacorporactivo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `subcuenca`
--
ALTER TABLE `subcuenca`
ADD CONSTRAINT `subcu` FOREIGN KEY (`idcuenca`) REFERENCES `cuenca` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `valorpa`
--
ALTER TABLE `valorpa`
ADD CONSTRAINT `proyvalor` FOREIGN KEY (`idproy`) REFERENCES `proyecto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `valorsubproy`
--
ALTER TABLE `valorsubproy`
ADD CONSTRAINT `finanproy` FOREIGN KEY (`idrecurso`) REFERENCES `financiacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `subprovalor` FOREIGN KEY (`idsub`) REFERENCES `proyecto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `zonificacion`
--
ALTER TABLE `zonificacion`
ADD CONSTRAINT `corr3` FOREIGN KEY (`idcorre`) REFERENCES `corregimiento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `cuenca3` FOREIGN KEY (`idcuenca`) REFERENCES `cuenca` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `mun3` FOREIGN KEY (`idmun`) REFERENCES `municipio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `subcuenca` FOREIGN KEY (`idsubcuenca`) REFERENCES `subcuenca` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
